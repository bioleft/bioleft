import { writable, get } from "svelte/store";
import { UserStore, to_decrypt } from "../store/user";
import { Bioleft } from "../bioleft";
import * as idb from 'idb-keyval';
import { goto } from '$app/navigation';

export interface ApiState {
    host: string;
    access_token: string;
    time: string;
}

const service = () => {

    let state: ApiState = {
        host: '',
        access_token: '',
        time: null
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async set_host(host){
            await update(s=>{
                s.host=host
                return s
            })
        },
        async token(url, user){
            let form_user = new FormData();
            form_user.append('username', user.username);
            form_user.append('password', user.password);
            let headers = {}
            let options = {
                method: "POST",
                body: form_user,
                headers: headers
            }
            let results = await fetch(
                state.host+url,
                options
            )
            let token = await results.json();
            if(token.access_token){
                let new_time = new Date()
                await update(s=>{
                    s.access_token = token.access_token,
                    s.time = new_time
                    return s
                })
            }
            return token
        },
        async profile(access){
            let results = await methods._get('/user/', true)
            if(results == undefined || results.status==401){
                await Bioleft.update(b=>{
                    b.account.logout = true;
                    return b
                })
                return false;
            }
            return results;
        },
        async geo_ref(access, geo){
            let results = await methods._post(
                '/user/georef',
                JSON.stringify(geo)
            )
            if(results.status==401)return;
            return results;
        },
        async _post (url, body, auth=true){
            return await methods._fetch(
                url,
                'POST',
                body,
                auth
            )
        },
        async _put (url, body, auth=true){
            return await methods._fetch(
                url,
                'PUT',
                body,
                auth
            )
        },
        async _delete (url, body, auth=true){
            return await methods._fetch(
                url,
                'DELETE',
                body,
                auth
            )
        },
        async _get (url, auth=true){
            return await methods._fetch(url, 'GET', null, auth)
        },
        async _fetch(url, method, body=null, auth=true){
            if(state.time != null){
                let user = get(UserStore)
                let diftime = (new Date(state.time) - new Date().getTime()) / 1000;
                if(diftime < -50){
                    let iv = await idb.get('iv');
                    let l = await idb.get('l');
                    let c = await idb.get('c');
                    let access = await methods.token(
                        "/user/token",
                        JSON.parse(await to_decrypt(l, iv, c))
                    )
                }
            }
            let headers = {}
            let options = {
                method: method
            }
            if (auth)
                headers['Authorization'] = 'Bearer '+state.access_token
            if (body!=null){
                if(typeof body == 'string')
                    headers['Content-Type'] = 'application/json'
                options.body = body
            }
            options.headers = headers
            let results = await fetch(
                state.host+url,
                options
            )
            if(results.status==401)return;
            return await results.json();
        }

    }

    return {
        subscribe,
        update,
        set,
        ...methods
    }
}

export let ServerApi = service()
