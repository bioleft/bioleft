const service = () => {

    const methods = {
        async window_(url){
            window.location = url
        },
        async location(){
            return window.location
        },
    }

    return {
        ...methods
    }
}

export let url = service()