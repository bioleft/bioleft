import { get } from 'svelte/store';
import { writable } from "svelte/store";
import { UserStore, to_decrypt } from "./store/user";
import { Seed } from "./store/seed";
import { ServerApi } from "./utils/server-api";
import * as idb from 'idb-keyval';

export interface BioleftState {
    ready: boolean;
    user_guest: boolean;
    system_message: string;
    account: {
        login: boolean;
        registry: boolean;
        logout: boolean;
        password_reset: boolean;
        password_change: boolean;
        init: boolean;
    },
    // Use for the value identifying the programmed view
    app_page: "default"
}

export function fdate(date){
    let ndate = new Date(date)
    return ndate.toLocaleDateString('es-ES')
}

const bioleft_main = () => {

    let state: BioleftState = {
        ready: false,
        user_guest: false,
        system_message: null,
        account: {
            login: false,
            registry: false,
            logout: false,
            password_reset: false,
            password_change: false,
            init: false,
            perfil: false,
        },
    }

    const { subscribe, set, update } = writable(state);

    const methods = {
        async init(data){
            let token = await idb.get('t');
            if(data.host!='')
                await ServerApi.set_host(data.host.trim());
            if(token){
                let iv = await idb.get('iv');
                let l = await idb.get('l');
                let c = await idb.get('c');
                let access = await UserStore.signIn(JSON.parse(await to_decrypt(l, iv, c)))
                if(!access){
                    return;
                }
                await ServerApi.update(s=>{
                    s.access_token = access.access_token
                    return s
                });
                let user = await ServerApi.profile(access.access_token)
                await UserStore.update(s=>{
                    s.profile = user
                    s.authorization = access.access_token
                    return s
                })
                if(!get(UserStore).ready){
                    await methods.load_user();
                    await UserStore.update(s=>{
                        s.ready = true
                        return s
                    })
                }
            }else{
                await update(b=>{
                    b.account.user_guest = true
                    return b
                })
            }
            await update(s=>{
                s.ready=true
                return s
            })
        },
        async load_user(){
            await UserStore.get_orgs()
            await UserStore.seeds()
            await UserStore.projects()
            await UserStore.fieldbooks()
        },
        async set_system_message(body){
            await update(s=>{
                s.system_message = body
                return s
            })
        }
    }

    return {
        subscribe,
        set,
        update,
        ...methods,
    };
};

export const Bioleft = bioleft_main()
