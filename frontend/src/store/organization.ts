import { get, writable } from "svelte/store";
import { UserStore } from "./user";
import { ServerApi } from "../utils/server-api";

export interface organization_store {
    id?: number;
    uuid?: string;
    name: string;
    registered_by_id?: number;
    created_date?: string;
    updated_date?: string;
    avatar?: string;
    team_id?: number;
    bio: string;
    site_url: string;
    type: string;
    geolocation_id?: number;
    geolocation?: Object;
    registered_by?: Object;
    team?: Object;
}

const organization_init = () => {

    let state: organization_store = {
        name: null,
        bio: null,
        site_url: null,
        type: null,
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async create(organization) {
            let results = await ServerApi._post(
                '/org/create',
                JSON.stringify(organization)
            )
            await update(s=>{
                s = results;
                return s;
            })
            return results;
        },
        async get_uuid(organization) {
            let results = await ServerApi._get(
                '/org/'+organization
            )
            await update(s=>{
                s = results;
                return s;
            })
            return results;
        },
        async add_user(organization, user) {
            let usr = await ServerApi._post(
                '/org/'+organization+'/useradd',
                JSON.stringify(user)
            )
            usr.user = user
            await update(s=>{
                s.team.users = [...s.team.users, usr]
                return s;
            })
            return usr;
        },
        async remove_user(organization, user) {
            let usr = await ServerApi._delete(
                '/org/'+organization+'/userdel',
                JSON.stringify(user)
            )
            await update(s=>{
                s.team.users = s.team.users.filter(u=>u.user_id!=user.user.id)
                return s;
            })
            return usr;
        },
    }
    return {
        subscribe,
        set,
        update,
        ...methods,
    };
}

export const Organization = organization_init();
