import { get } from 'svelte/store';
import { writable } from "svelte/store";
import { Project } from "./project";
import { UserStore } from "./user";
import { Seed } from "./seed";
import { ServerApi } from "../utils/server-api";

export interface fieldbook_store {
    form: string;
    crop: string;
    obj?: string;
    title?: string;
    managment?: string;
    id?: number;
    project_id: number;
    project?: null;
    items?: Array<fieldbook_item_store>;
    crop_plan?: fieldbook_crop_store;
}

export interface fieldbook_crop_settings_store {
    fieldbooks: Array<fieldbook_store>;
    fieldbook: fieldbook_store;
    trail_init_instance: boolean;
    trail_environment_instance: boolean;
    trail_item_instance: boolean;
    current_cycle: string;
}

export interface fieldbook_item_store {
    id: number;
    name: string;
    title: string;
    alias: string;
    description: string;
    text_label: string;
    type_format: string;
    fieldbook_id: number;
    category_format: string; 
    optional: boolean;
    enable: boolean;
    options: string; 
    crop_cycle: string; 
    form_order: number;
}

export interface fieldbook_crop_environment_store {
    value: Array<string>;
}

export interface fieldbook_crop_store {
    environment: fieldbook_crop_environment_store;
    cycles: Array<string>;
    plan: Array<plan_store>;
}

export interface plan_store {
    cycle: string;
    items: Array<any>;
}

export interface trail_store{
    id: number;
    uuid: string;
    created_date: string;
    creator_id: number;
    fieldbook_id: number;
    name: string;
    environment: trail_environment_store;
    seeds: Array<any>;
    trail_seeds: Array<trail_seeds_store>;
    data: Array<trail_fieldbook_data_store>;
}

export interface trail_environment_store{
    id: number;
    trail_id: number;
    value: string;
}

export interface trail_seeds_store{
    id: number;
    trail_id: number;
    seed_id: number;
}

export interface trail_fieldbook_data_store{
    id: number;
    trail_id: number;
    item_id: number;
    created_date: string;
    date_observation: string;
    creator_id: number;
    trailseeds_id: number;
    value: string;
}

const trail_main = () => {
    let state: trail_store = {
        id: 0,
        uuid: null,
        created_date: null,
        creator_id: 0,
        fieldbook_id: 0,
        name: null,
        environment: null,
        seeds: [],
        trail_seeds: [],
        data: [],
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async set_fieldbook(fieldbook) {
            return update(s=>{
                s.fieldbook_id=fieldbook.id
                return s
            })
        },
        created(fieldbook_id) {
            return state.fieldbook_id == fieldbook_id ? true : false;
        },
        async create(fieldbook_id){
            let trail_server = await ServerApi._post(
                '/trail/init/'+fieldbook_id,
                {}
            )
            let state_: trail_store = trail_server;
            state_.seeds = []
            state_.trail_seeds = []
            state_.data = []
            state_.environment = null
            await update(s=>s=state_);
            await UserStore.update(s=>{
                s.trails = s.trails.concat([trail_server])
                return s
            })
        },
        async set_environment(value){
            let state = get(Trail)
            let env_state = {
                trail_id: state.id,
                value: value
            };
            let trail_env_state = await ServerApi._post(
                '/trail/'+state.uuid+'/environment',
                JSON.stringify(env_state),
            )
            let env_state_: trail_environment_store = trail_env_state;
            update(s=>{
                s.environment = env_state_
                return s
            })
        },
        async _save_item(item){
            let state = get(Trail)
            let trail_data_server = await ServerApi._post(
                '/trail/'+state.uuid+'/data',
                JSON.stringify(item),
            )
            return trail_data_server;
        },
        async _update_item(item){
            let state = get(Trail)
            let trail_data_server = await ServerApi._put(
                '/trail/'+state.uuid+'/data',
                JSON.stringify(item),
            )
            return trail_data_server;
        },
        async save_item(item, seed, value){
            let user_state = get(UserStore);
            let state = get(Trail)
            let trailseed = state.trail_seeds.filter(ts=>ts.seed_id==seed.id)[0]
            let data_state = state.data.filter(ts=>
                ts.trailseeds_id==trailseed.id &&
                ts.item_id==item.id
            )
            if(data_state.length){
                let ndata_state = data_state[0]
                if(item.type_format == 'optional_multiple'){
                    if(ndata_state.value.search(value)>=0){
                        if(ndata_state.value.length==value.length){
                            ndata_state.value = ""
                        }else{
                            let and = ndata_state.value.split(',')
                            let nand = [];
                            for (let i = 0; i < and.length; i++) {
                                if(and[i] != value){
                                    nand.push(and[i])
                                }
                            }
                            ndata_state.value = nand.toString();
                        }
                    }else{
                        ndata_state.value = ndata_state.value+","+value
                    }
                }else{
                    ndata_state.value = value
                }
                await methods._update_item(ndata_state);
                update(s=>{
                    s.data.splice(
                        s.data.indexOf(data_state[0]),1,
                        ndata_state
                    )
                    return s
                })
            }else{
                let trail_data: trail_fieldbook_data_store = {
                    trail_id: state.id,
                    item_id: item.id,
                    creator_id: user_state.profile.id,
                    trailseeds_id: trailseed.id,
                    value: value,
                    id: null,
                    created_date: null,
                    date_observation: null,
                }
                trail_data = await methods._save_item(trail_data);
                update(s=>{
                    s.data.push(trail_data);
                    return s
                })
            }
        },
        async has_data(item, seed, value){
            let state = get(Trail)
            let trailseed = state.trail_seeds.filter(ts=>ts.seed_id==seed.id)[0]
            let data_state = state.data.filter(ts=>
                ts.trailseeds_id==trailseed.id &&
                ts.item_id==item.id &&
                ts.value==value
            )
            if(data_state.length){
                return true
            }
            return false
        },
        get_data(item, seed){
            let state = get(Trail)
            let trailseed = state.trail_seeds.filter(ts=>ts.seed_id==seed.id)[0]
            let data_state = state.data.filter(ts=>
                ts.trailseeds_id==trailseed.id &&
                ts.item_id==item.id
            )
            if(data_state.length){
                return data_state[0]
            }
            return {
                value: ''
            }
        },
        async get_trail(uuid){
            let trail = await methods.trail_server(uuid)
            let seeds = [];
            trail.trail_seeds.forEach(ts=>seeds.push(ts.seed))
            await update(s=>{
                s = trail,
                s.seeds = seeds
                return s
            });
        },
        async trail_server(uuid){
            let trail_data_server = await ServerApi._get(
                '/trail/'+uuid,
                true,
            )
            return trail_data_server
        },
        async trail_seeds(){
            let trail_state = get(Trail);
            let user_state = get(UserStore);
            let seed_state = get(Seed)
            let seeds = [];
            trail_state.trail_seeds.forEach(ts=>seeds.push(ts.seed))
            await update(t=>{
                t.seeds = seeds
                return t
            })
        }
    }
    return {
        subscribe,
        set,
        update,
        ...methods,
      };
}

export const Trail = trail_main();

const fieldbook_init = () => {

    let state: fieldbook_crop_settings_store = {
        fieldbooks: [],
        fieldbook: {
            form: '',
            crop: '',
            project_id: 0,
        },
        trail_init_instance: false,
        trail_environment_instance: false,
        trail_item_instance: false,
        current_cycle: null,
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async get_form(form, crop){
            crop = crop.replace(' ', '-')
            let results = await ServerApi._get(
                '/fieldbook/'+form+'/'+crop,
            )
            return results
        },
        async generate_crop_plan(fieldbook, item){
            let item_: fieldbook_item_store = item;
            let cycles = fieldbook.crop_plan.cycles.find(c => c==item.crop_cycle)
            if(cycles){
                let plan = fieldbook.crop_plan.plan.find(p => p.cycle==cycles);
                plan.items.push(item_)
                plan.items = plan.items.sort((i,x)=>i.form_order-x.form_order)
            }else{
                let plan = fieldbook.crop_plan.plan.find(p => p.cycle==item.crop_cycle);
                if (plan != undefined){
                    plan.items.push(item_)
                    plan.items = plan.items.sort((i,x)=>i.form_order-x.form_order)
                }else{
                    let plan = {
                        cycle: item.crop_cycle,
                        items: [item_],
                    }
                    fieldbook.crop_plan.plan.push(plan)
                    fieldbook.crop_plan.cycles.push(item.crop_cycle)
                    plan.items = plan.items.sort((i,x)=>i.form_order-x.form_order)
                }
            }
        },
        async configure(fieldbook) {
            let fc_environment: fieldbook_crop_environment_store = {
                value: [
                    'A campo en suelo',
                    'Bajo cubierta en suelo',
                    'A campo en contenedores',
                    'Bajo cubierta en contenedores',
                ]
            }
            let crop_plan: fieldbook_crop_store = {
                environment: fc_environment,
                cycles: [],
                plan: [],
            }
            fieldbook.crop_plan = crop_plan;
            let items_ = fieldbook.items.sort((i,x)=>i.form_order-x.form_order)
            for (const i of items_.values())
                methods.generate_crop_plan(fieldbook, i);
            return update(state => {
                state.fieldbook = fieldbook
                return state
            })
        },
        async add_item(item){
            let project_state = get(Project);

            let item_server = await ServerApi._post(
                '/project/'+project_state.project.uuid+'/fieldbook/'+state.fieldbook.form,
                JSON.stringify(item),
            )

            let _item: fieldbook_item_store = item_server;

            return update(s=>{
                if(s.fieldbook.items == undefined){
                    s.fieldbook.items = [_item]
                }else{
                    s.fieldbook.items = [...s.fieldbook.items, _item]
                }
                return s
            })
        },
        async remove_item(item){
            let project_state = get(Project);
            let item_server = await ServerApi._delete(
                '/project/'+project_state.project.uuid+'/fieldbook/'+state.fieldbook.form+'/'+item.name,
                JSON.stringify(item),
            )
            return update(s=>{
                s.fieldbook.items = s.fieldbook.items.filter(i=>i.id!=item.id)
                return s
            })
        },
        async add_fieldbook(fieldbook) {
            let fieldbook_store: fieldbook_store = fieldbook;
            methods.configure(fieldbook_store);
            update(state => {
                state.fieldbooks.push(fieldbook_store)
                return state
            })
            return fieldbook_store
        },
        async set_fieldbook(fieldbook){
            return update(state => {
                state.fieldbook = fieldbook
                return state
            })
        },
        async init_crop() {
            let trail_state = get(Trail)
            if (trail_state.fieldbook_id == undefined || trail_state.fieldbook_id != state.fieldbook.id){
                return update(state => {
                    state.trail_init_instance=true
                    //state.current_cycle=state.fieldbook.crop_plan.cycles[0]
                    return state
                })
            }else{
                methods.environment_instance();
            }
        },
        async environment_instance(){
            return update(state => {
                state.trail_init_instance=false
                state.trail_item_instance=false
                state.trail_environment_instance=true
                state.current_cycle=state.fieldbook.crop_plan.cycles[0]
                return state
            })
        },
        async init_trail() {
            await Trail.create(state.fieldbook.id);
            update(state => {
                state.trail_init_instance=false
                state.trail_environment_instance=true
                state.current_cycle=state.fieldbook.crop_plan.cycles[0]
                return state
            })
        },
        async next_cycle() {
            let inc = state.fieldbook.crop_plan.cycles.indexOf(state.current_cycle)
            if(state.fieldbook.crop_plan.cycles[inc+1]==undefined){
                return update(state => {
                    state.trail_init_instance=true
                    state.trail_environment_instance=false
                    state.trail_item_instance=false
                    return state
                })
            }
            return update(state => {
                state.current_cycle=state.fieldbook.crop_plan.cycles[inc+1]
                return state
            })
        },
        async prev_cycle() {
            let inc = state.fieldbook.crop_plan.cycles.indexOf(state.current_cycle)
            if (inc != 0){
                return update(state => {
                    state.current_cycle=state.fieldbook.crop_plan.cycles[inc-1]
                    return state
                })
            }else{
                return update(state => {
                    state.trail_environment_instance=true
                    state.trail_item_instance=false
                    return state
                })
            }
        },
        async add_seed_trail(seed){
            let trail_state = get(Trail)
            let user_state = get(UserStore);
            let trail_seed = {
                trail_id: trail_state.id,
                seed_id: seed.id,
            }
            let trail_seed_server = await ServerApi._post(
                '/trail/'+trail_state.uuid+'/seed',
                JSON.stringify(trail_seed),
            )
            let tss: trail_seeds_store = trail_seed_server;
            await Trail.update(ts=>{
                ts.trail_seeds = ts.trail_seeds.concat([tss])
                return ts
            })
            await Trail.trail_seeds();
        },
        async remove_seed_trail(tseed){
            let trail_state = get(Trail)
            let user_state = get(UserStore);
            let trail_seed_server = await ServerApi._delete(
                '/trail/'+trail_state.uuid+'/seed',
                JSON.stringify(tseed),
            )
            let tss: trail_seeds_store = trail_seed_server;
            await Trail.update(ts=>{
                ts.trail_seeds.splice(ts.trail_seeds.indexOf(tseed),1)
                return ts
            })
            await Trail.trail_seeds();
        },
        async end_env_trail(){
            update(state => {
                state.trail_environment_instance=false
                state.trail_item_instance=true
                return state
            })
        },
        refresh_form(){
            let fieldbook = get(Fieldbook)
            let _plan = fieldbook.fieldbook.crop_plan.plan.filter(
                (plan)=>plan.cycle==fieldbook.current_cycle
            )[0];
            return _plan
        },
        current_form_items() {
            let _plan = state.fieldbook.crop_plan.plan.filter(
                (plan)=>plan.cycle==state.current_cycle
            )[0];
            return _plan.items;
        },
        async save_item(item){
            let server = get(ServerApi)
            let user_state = get(UserStore);
            let project_state = get(Project);
            let item_server = await ServerApi._put(
                '/project/'+project_state.project.uuid+'/fieldbook/'+state.fieldbook.form+'/'+item.name,
                JSON.stringify(item),
            )
        }
    };

    return {
      subscribe,
      set,
      update,
      ...methods,
    };
  };

  export const Fieldbook = fieldbook_init();
