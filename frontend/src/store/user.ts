import { writable, get } from "svelte/store";
import { ServerApi } from '../utils/server-api';
import { Seed } from "./seed";
import * as idb from 'idb-keyval';

async function sha256(value) {
    return await crypto.subtle.digest(
        'SHA-256',
        new TextEncoder().encode(value)
    );
}

async function getKey(password) {
    return await crypto.subtle.importKey(
        'raw',
        await sha256(password),
        {name: 'AES-GCM'},
        false,
        ['encrypt', 'decrypt']
    );
}

async function to_crypt(iv, key, password){
    const encrypted = await crypto.subtle.encrypt(
        {
            iv,
            name: 'AES-GCM'
        },
        key,
        new TextEncoder().encode(password)
    );
    return encrypted
}

export async function to_decrypt(key, iv, encrypted){
    let decrypted = await crypto.subtle.decrypt(
        {
            iv,
            name: 'AES-GCM'
        },
        key,
        encrypted
    );

    return new TextDecoder().decode(decrypted);
}

async function save_session(user, uncrypted, access){
    const iv = crypto.getRandomValues(new Uint8Array(12));
    const key = await getKey(user)
    const cr = await to_crypt(iv, key, uncrypted);
    idb.set('iv', iv);
    idb.set('l', key);
    idb.set('c', cr);
    idb.set('t', access)
}

async function didb(){
    idb.set('iv', null);
    idb.set('l', null);
    idb.set('c', null);
    idb.set('t', null)
}

export interface IUserState {
    ready: boolean;
    signedIn?: boolean;
    authorization: string;
    profile: {
      id: number;
      username?: string;
      email?: string;
      password?: string;
      name: string;
      bio: string;
      contact: string;
      geolocation: {
        location: string;
        display_name: string;
        latitude: string;
        longitude: string;
      };
      interest_activities?: [];
      interest_crop?: [];
      products_offered?: [];
    };
    organizations?: Array<any>;
    projects?: Array<any>;
    fieldbooks?: Array<any>;
    trails?: Array<any>;
    requests?: Array<any>;
    view?: Object;
  }

const userInit = () => {

    let state: IUserState = {
      ready: false,
      signedIn: false,
      authorization: null,
      profile: {
        id: 0,
        username: null,
        email: null,
        password: null,
        name: null,
        bio: null,
        contact: null,
        geolocation: {
          location: null,
          display_name: null,
          latitude: null,
          longitude: null,
        },
      },
      organizations: [],
      projects: [],
      fieldbooks: [],
      trails: [],
      requests: [],
      view: {}
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async signIn(user) {
            let access = await ServerApi.token(
                '/user/token',
                user
            );
            if(access.access_token != undefined)
                await methods.access_profile(user, access);
            return access
        },

        async signInSocial(user) {
            let access = await ServerApi.token(
                '/user/token_social',
                user
            );
            if(access.access_token != undefined)
                await methods.access_profile(user, access);
            return access
        },

        async access_profile(user, access){
            let profile = await ServerApi.profile(access.access_token);

            if(!profile)
                return;

            user.password = profile.password;

            save_session(
                profile.password,
                JSON.stringify(user),
                access.access_token
            )

            if(profile.email == user.username){
                await update(s=>{
                    s.profile = profile
                    s.authorization = access.access_token
                    return s
                })
            }
        },

        async change_password(current, newp){
            let user_password = {
                password: current,
                npassword: newp
            }
            let results = await ServerApi._post(
                '/user/changepassword',
                JSON.stringify(user_password)
            )
            return results;
        },

        async registry(user){
            let results = await ServerApi._post(
                '/user/registry',
                JSON.stringify(user.profile),
                false
            )
            let user_ = results;
            if(user_.id != undefined){
                return update(s=>{
                    s.profile=user_
                    return s
                })
            }
            return user_
        },

        async logout(){
            let state: IUserState = {
                ready: false,
                signedIn: false,
                authorization: null,
                profile: {
                    id: 0,
                    username: null,
                    email: null,
                    password: null,
                    name: null,
                    bio: null,
                    contact: null,
                    geolocation: {
                        location: null,
                        display_name: null,
                        latitude: null,
                        longitude: null,
                    },
                },
                organizations: [],
                projects: [],
                fieldbooks: [],
                requests: []
            };
            set(state);
            await didb();
        },

        async set_user(state){
            return update(s=>state)
        },

        async get_orgs(){
            let results = await ServerApi._get('/user/orgs', true)
            await update(s=>{
                s.organizations=results
                return s
            })
            return results;
        },

        async get_username(username){
            let results = await ServerApi._get('/user/getusername/'+username, true)
            return results;
        },

        async seeds(username=null){
            let store = get(UserStore)
            if(username==null)
                username = store.profile.username
            let results = await ServerApi._get('/catalog/'+username, true)
            let seeds = results;
            if(username==store.profile.username)
                await Seed.update(s=>{
                    s.user=seeds
                    return s
                })
            return seeds;
        },

        async projects(){
            let store = get(UserStore)
            let results = await ServerApi._get('/project/user/'+store.profile.username, true)
            let projects = results
            update(s=>{
                s.projects = projects
                return s
            })
            return projects
        },

        async fieldbooks(username=null){
            let store = get(UserStore)
            let list_fieldbooks = []
            for (const key in store.projects) {
                const prj = store.projects[key];
                let results = await ServerApi._get('/project/'+prj.uuid+'/fieldbooks', true)
                let fieldbooks = results
                for (const key in fieldbooks) {
                    const element = fieldbooks[key];
                    await methods.trails(element)
                    list_fieldbooks.push(element)
                }
            }
            await update(s=>{
                s.fieldbooks = list_fieldbooks
                return s
            })
        },

        async get_requests(){
            let results = await ServerApi._get('/user/requests', true)
            let attributes = results
            await update(s=>{
                s.requests = attributes
                return s
            })
            return attributes;
        },

        async get_offers(){
            let offers = [];
            let seeds = get(Seed)
            for (const key in seeds.user) {
                const element = seeds.user[key];
                let offer = Seed.get_requests(element.owner.username, element)
                offers = offers.concat(offer)
            }
            return offers
        },

        async trails(fieldbook){
            let store = get(UserStore)
            let results = await ServerApi._get('/trail/fieldbook/'+fieldbook.uuid, true)
            let trails = []
            if(results.length>0){
                if(store.trails != undefined && store.trails.length>0){
                    trails = store.trails.concat(results)
                }else{
                    trails = results
                }
                await update(s=>{
                    s.trails = trails
                    return s
                })
            }
        },

        async geo_ref(geo){
            let georef = {
                loc: geo,
                reg: "ar",
                limit: 5,
            }
            let geoserver = await ServerApi.geo_ref(state.authorization, georef)
            return geoserver;
        },

        async save(){
            let results = await ServerApi._post(
                '/user/config',
                JSON.stringify(get(UserStore).profile)
            )
            let user_ = results;
            return update(s=>{
                s.profile=user_
                return s
            })
        },

        async getid(id){
            let results = await ServerApi._get('/user/getid/'+id, true)
            return results;
        },

        async search(username){
            let results = await ServerApi._get('/user/list/'+username, true)
            return results;
        },

        async save_image(image){
            const formData = new FormData()
            formData.append('file', image)
            let results = await ServerApi._post(
                '/user/image',
                formData
            )
            let avatar = results;
            return update(s=>{
                s.profile['avatar']=avatar
                return s
            })
        },

        signout() {},

        async password_reset(user){
            let results = await ServerApi._post(
                '/user/reset',
                JSON.stringify(user)
            )
            return results;
        },

        async password_recovery(user){
            let results = await ServerApi._post(
                '/user/recovery',
                JSON.stringify(user)
            )
            return results;
        },
    };

    return {
        subscribe,
        set,
        update,
        ...methods,
    };
};

export const UserStore = userInit();
