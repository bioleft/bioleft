import { get } from 'svelte/store';
import { writable } from "svelte/store";
import { UserStore } from "./user";
import { ServerApi } from "../utils/server-api";

export interface community_search {
    search: string;
    species: string;
    activities: string;
    products_offered: string;
}

export interface topic_store {
    id: number;
    title: string;
    created_date: string;
    created_user_id: number;
    visits: number;
    content: string;
    category: string;
    replies: Array<reply>;
}

export interface reply {
    id: number;
    topic_id: number;
    created_date: string;
    created_user_id: number;
    content: string;
}

const community_init = () => {

    let state: community_search = {
        search: '',
        species: null,
        activities: null,
        products_offered: null
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async search() {
            let params = ["?"]
            params.push("search="+state.search)
            if(state.species)
                params.push("species="+state.species)
            if(state.activities)
                params.push("activities="+state.activities)
            if(state.products_offered)
                params.push("products_offered="+state.products_offered)
            for (let i = 2; i < params.length; i++) {
                params[i] = "&"+params[i]
            }
            let server_community = await ServerApi._get(
                '/community/data'+params.join('').replace(',',''), false
            )
            return server_community;
        },
    }
    return {
        subscribe,
        set,
        update,
        ...methods,
      };
}

export const Community = community_init();

const forum_init = () => {

    let state = {
        topic: null,
        topics: [],
        reply: null
    };

    const { subscribe, set, update } = writable(state);

    const methods = {
        async add_topic(topic){
            let user_state = get(UserStore);
            let item_server = await ServerApi._post(
                '/forum/topic',
                JSON.stringify(topic)
            )
            topic = item_server
        },
        async set_topic(topic){
            return update(e=>{
                e.topic = topic
                return e
            })
        }
    }

    return {
      subscribe,
      set,
      update,
      ...methods,
    };
};

export const Forum = forum_init();
