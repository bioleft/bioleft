import { writable, get } from "svelte/store";
import { UserStore } from "./user";
import { Fieldbook } from './fieldbook';
import { ServerApi } from "../utils/server-api";

export interface project_store{
    user: Array<any>;
    project: {
        name: string;
        description?: string;
        organization_id?: number;
        managment?: string;
        team_id?: number;
        team?: Array<any>
        id?: number;
        uuid?: string;
        organization?: Object;
    }
}

const project_main = () => {

    let state: project_store = {
        user: [],
        project: {
            name: "",
            description: null,
        }
    }

    const { subscribe, set, update } = writable(state);

    const methods = {
        async init(uuid) {
            let results = await ServerApi._get(
                '/project/'+uuid,
                true
            )
            let state = results
            return update(s=>{
                s.project = state
                return s
            })
        },
        async create(project){
            let project_server = await ServerApi._post(
                '/project/create',
                JSON.stringify(project)
            )
            update(s=>{
                s.project = project_server
                s.user = [...s.user, project_server]
                return s
            })
            return project_server
        },
        async put(project){
            let project_server = await ServerApi._put(
                '/project/'+project.uuid,
                JSON.stringify(project),
                true
            )
            update(s=>{
                s.project = project
                return s
            })
            return project
        },
        async delete(project){
            let project_server = await ServerApi._delete(
                '/project/'+project.uuid,
                JSON.stringify(project),
                true
            )
            update(s=>{
                s.project = {
                    id: undefined,
                    uuid: null,
                    name: null,
                }
                return s
            })
            return project
        },
        async get_user(user) {
            let user_state = get(UserStore);
            let results = await ServerApi._get(
                '/project/user/'+user.username,
                true
            )
            return results
        },
        async get(uuid) {
            let user_state = get(UserStore);
            let results = await ServerApi._get(
                '/project/'+uuid,
                true
            )
            return results
        },
        async add_user(project, user){
            let prjusr_server = await ServerApi._post(
                '/project/'+project.uuid+'/team',
                JSON.stringify(user),
            )
            let usr = prjusr_server;
            if(state.project.team == undefined){
                state.project.team = [usr]
            }else{
                state.project.team.users = [...state.project.team.users, usr]
            }
            update(s=>{
                s.project.team.users = state.project.team.users
                return s
            })
            return project
        },
        async remove_user(project, user){
            let prjusr_server = await ServerApi._delete(
                '/project/'+project.uuid+'/team',
                JSON.stringify(user),
                true
            )
            update(s=>{
                s.project.team.users = s.project.team.users.filter(u=>u.user_id!=user.id)
                return s
            })
            return prjusr_server
        },
        async create_fieldbook(fieldbook){
            let project_server = await ServerApi._post(
                '/project/'+state.project.uuid+"/fieldbooks",
                JSON.stringify(fieldbook)
            )
            return project_server;

        },

    }
    return {
        subscribe,
        set,
        update,
        ...methods,
      };
}

export const Project = project_main();
