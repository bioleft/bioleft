import { writable, get } from "svelte/store";
import { UserStore } from "./user";
import { ServerApi } from "../utils/server-api";

export interface seed_store{
    id: number;
    uuid: string;
    cultivar: string;
    creator_id: number;
    owner_id: number;
    organization_id?: number;
    species_id: number;
    licence_id: number;
    type_: number;
    detail: string;
    created_from_id?: number;
    experimental: boolean;
    species?: {};
    licence?: {
        name: string;
        url: string;
        id: number;
    };
    image_primary?: string;
    breeder?: string;
    attributes?: Array<any>;
}

interface offer_config{
    option: string;
    offered: number;
    location: string;
    terms: string;
    offered_per_person: number;
    geolocation_id?: number;
}

export interface bioleft_store{
    catalog: Array<any>;
    user: Array<seed_store>;
    data: seed_store;
    offer: offer_config;
    offer_setup: false;
    request_setup: false;
}

const seed_main = () => {

    let state: bioleft_store = {
        catalog: [],
        user: [],
        data: {
            id: 0,
            uuid: '',
            cultivar: '',
            creator_id: 0,
            owner_id: 0,
            organization_id: 0,
            species_id: 0,
            licence_id: 0,
            type_: 0,
            detail: '',
            created_from_id: 0,
            experimental: false,
            species: {},
            licence: {
                name: null,
                url: null,
                id: null
            },
            image_primary: '',
            breeder: '',
            attributes: []
        },
        offer: {
            option: '',
            offered: 0,
            terms: '',
            location: '',
            offered_per_person: 0
        },
        offer_setup: false,
        request_setup: false
    }

    const { subscribe, set, update } = writable(state);

    const methods = {
        async update_catalog(filters={}){
            let params = ["?"]
            if(filters.species != [] && filters.species != undefined)
                params.push("species="+filters.species.join(','))
            if(filters.cultivar)
                params.push("cultivar="+filters.cultivar)
            if(filters.location)
                params.push("location="+filters.location)
            if(filters.licence)
                params.push("licence="+filters.licence)
            if(filters.type)
                params.push("type_="+filters.type)
            if(filters.order)
                params.push("order="+filters.order)

            for (let i = 2; i < params.length; i++) {
                params[i] = "&"+params[i]
            }
            let results = await ServerApi._get(
                '/catalog/'+params.join(''), 
                false
            )
            await update(s=>{
                s.catalog=results
                return s
            })
        },
        async set_seed(seed){
            await update(s=>{
                s.data=seed
                return s
            })
        },
        async crop_techsheet(crop){
            let user = get(UserStore)
            let results = await ServerApi._get('/seed/crop/'+crop, true)
            return results;
        },
        async search(account, seed){
            let results = await ServerApi._get('/catalog/'+account+"/"+seed, true)
            return results;
        },
        async get_user(user){
            let results = await ServerApi._get(
                '/catalog/'+user.username,
                true
            )
            return results;
        },
        async get_by_uuid(uuid){
            let results = await ServerApi._get(
                '/catalog/uuid/'+uuid,
                true
            )
            return results;
        },
        async get_species(){
            let results = await ServerApi._get(
                '/seed/species',
                false
            )
            return results;
        },
        async filter(seed){
            let results = await ServerApi._post(
                '/seed/filter_cultivar',
                JSON.stringify(seed),
            )
            return results;
        },
        async register(seed){
            let user = get(UserStore)
            let results = await ServerApi._post(
                '/seed/register',
                JSON.stringify(seed),
            )
            return results;
        },
        async add_seed(seed, image=null){
            let seeds = await methods.register(seed)
            if(image){
                await methods.save_image(seeds, image);
                seeds.image_primary = image.name;
            }
            await update(s=>{
                s.user.push(seeds);
                return s
            })
            return seeds
        },
        async update_seed(seed){
            let _seed = seed;
            delete _seed.species;
            delete _seed.licence;
            delete _seed.attributes;
            let results = await ServerApi._put(
                '/seed/'+seed.owner.username+"/"+seed.uuid,
                JSON.stringify(seed),
                true
            )
            update(s=>{
                s.data = results
                return s
            })
            return results
        },
        async save_image(seed, image){
            let user = get(UserStore)
            const formData = new FormData()
            formData.append('file', image)
            let results = await ServerApi._post(
                '/seed/'+user.profile.username+'/'+seed.uuid+'/image',
                formData
            )
            return results;
        },
        async remove_seed(account, seed){
            let seed_server = await ServerApi._delete(
                '/seed/remove/'+account+"/"+seed.uuid,
                JSON.stringify(seed),
                true
            )
            return seed_server;
        },
        async request(account, seed, request){
            let results = await ServerApi._post(
                '/seed/'+account+"/"+seed.uuid+'/request',
                JSON.stringify(request)
            )
            return results;
        },
        async update_request(account, seed, request){
            let seed_server = await ServerApi._put(
                '/seed/'+account+"/"+seed.uuid+'/request',
                JSON.stringify(request),
                true
            )
            return seed_server;
        },
        async offer(account, seed, data){
            let seed_server = await ServerApi._post(
                '/seed/'+account+"/"+seed.uuid+'/offer',
                JSON.stringify(data),
                true
            )
            return seed_server;
        },
        async get_offer(account, seed){
            let results = await ServerApi._get(
                '/seed/'+account+"/"+seed.uuid+'/offer',
                true
            )
            if(results){
                await update(s=>{
                    s.offer = results
                    return s
                })
            }
            if(results == null){
                await update(s=>{
                    s.offer = {}
                    return s
                })
            }
            return results;
        },
        async get_requests(account, seed){
            let results = await ServerApi._get(
                '/seed/'+account+"/"+seed.uuid+'/request',
                true
            )
            return results;
        },
        async update_offer(account, seed, offer){
            let seed_server = await ServerApi._put(
                '/seed/'+account+"/"+seed.uuid+'/offer',
                JSON.stringify(offer),
                true
            )
            return seed_server;
        },
        async add_attribute(account, seed, attribute){
            let seed_server = await ServerApi._post(
                '/seed/'+account+"/"+seed.uuid+'/attribute',
                JSON.stringify(attribute)
            )
            return seed_server;
        },
        async update_attribute(account, seed, attribute){
            let seed_server = await ServerApi._put(
                '/seed/'+account+"/"+seed.uuid+'/attribute',
                JSON.stringify(attribute),
                true
            )
            return seed_server;
        },
        async delete_attribute(account, seed, attribute){
            let seed_server = await ServerApi._delete(
                '/seed/'+account+"/"+seed.uuid+'/attribute',
                JSON.stringify(attribute),
                true
            )
            return seed_server;
        },
        async get_attributes(account, seed){
            let seed_server = await ServerApi._get(
                '/seed/'+account+"/"+seed.uuid+'/attributes',
                true
            )
            return seed_server;
        },
        async load_offer_setup(){
            await Seed.update(s=>{
                s.offer_setup = true
                return s
            })
        },
        async unload_offer_setup(){
            await Seed.update(s=>{
                s.offer_setup = false
                return s
            })
        },
        async load_request_setup(){
            await Seed.update(s=>{
                s.request_setup = true
                return s
            })
        },
        async unload_request_setup(){
            await Seed.update(s=>{
                s.request_setup = false
                return s
            })
        }
    }
    return {
        subscribe,
        set,
        update,
        ...methods,
      };
}

export const Seed = seed_main();
