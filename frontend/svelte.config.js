import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-node';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
      adapter: adapter({ out: 'build' }),
      serviceWorker: {
          register: false,
      },
      files: {
          assets: 'static',
          lib: 'src/lib',
          routes: 'src/routes',
          template: 'src/app.html',
          serviceWorker: 'src/service-worker.ts'
      }
	}
};

export default config;
