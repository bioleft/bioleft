from pydantic import BaseModel
from typing import Optional, List

class Catalog(BaseModel):
    cultivar: Optional[str] = None
    species: Optional[str] = None
    location: Optional[str] = None
    licence: Optional[int] = None
    order: Optional[str] = None
    type: Optional[str] = None

class Licence(BaseModel):
    url: str
    id: Optional[int] = None
    name: Optional[str] = None
    detail: Optional[str] = None
    image: Optional[str] = None

class Species(BaseModel):
    id: Optional[int] = None
    name: str

class SpeciesImgField(Species):
    image: str

class Seedattributes(BaseModel):
    id: Optional[int] = None
    name: str
    relevant: Optional[bool] = None
    created_date: Optional[str] = None
    updated_date: Optional[str] = None
    group: Optional[str] = None
    options: Optional[str] = None
    description: Optional[str] = None

class Seedattributespecies(BaseModel):
    id: Optional[int] = None
    species_id: Species
    attribute_id: Seedattributes

class Seed(BaseModel):
    id: Optional[int] = None
    cultivar: str
    species_id: Optional[int] = None
    creator_id: Optional[int] = None
    owner_id: Optional[int] = None
    licence_id: Optional[int] = None
    created_from_id: Optional[int] = None
    type_: Optional[str] = None
    detail: Optional[str] = None
    experimental: Optional[bool] = None
    breeder: Optional[str] = None
    is_transferred: Optional[bool] = None
    is_availables: Optional[bool] = None
    organization_id: Optional[int] = None
    visible_search: Optional[bool] = None
    created_date: Optional[str] = None
    transfer_settings_id: Optional[int] = None

    class Config:
        orm_mode = True

class SeedUuid(Seed):
    uuid: int

class Seedattribute(BaseModel):
    id: Optional[int] = None
    seed_id: Optional[int] = None
    attribute_id: Optional[int] = None
    attribute_by_user: Optional[str] = None
    name: Optional[str] = None
    value: Optional[str] = None
    unit: Optional[str] = None

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: Optional[str] = None

class Geo(BaseModel):
    loc: Optional[str] = None
    reg: Optional[str] = None
    limit: Optional[int] = None

class UserGeo(BaseModel):
    location: Optional[str] = None
    display_name: Optional[str] = None
    latitude: Optional[str] = None
    longitude: Optional[str] = None

class User(BaseModel):
    id: Optional[int] = None
    username: str
    email: Optional[str] = None
    name: Optional[str] = None
    bio: Optional[str] = None
    contact: Optional[str] = None
    geolocation: Optional[UserGeo] = None
    interest_activities: Optional[str] = None
    interest_crop: Optional[str] = None
    products_offered: Optional[str] = None

class UserInDB(User):
    password: str

class UserAvatar(User):
    avatar: Optional[str] = None

class UserPassword(BaseModel):
    password: str
    npassword: str

class Team(BaseModel):
    id: Optional[int] = None
    uuid: Optional[str] = None

class TeamUser(BaseModel):
    id: Optional[int] = None
    user_id: Optional[int] = None
    team_id: Optional[int] = None
    access_level: Optional[str] = None

class SeedTransferConfig(BaseModel):
    id: Optional[int] = None
    option: str
    offered: int
    terms: Optional[str] = None
    location: Optional[str] = None
    offered_per_person: int
    seed_id: int
    geolocation_id: Optional[int] = None

class SeedTransferRequest(BaseModel):
    id: Optional[int] = None
    user_request_id: int
    option: Optional[str] = None
    offer_id: Optional[int] = None
    seed_id: Optional[int] = None
    seed_quantity: int
    status: Optional[str] = None

class Organization(BaseModel):
    id: Optional[int] = None
    uuid: Optional[str] = None
    name: str
    registered_by_id: Optional[str] = None
    created_date: Optional[str] = None
    updated_date: Optional[str] = None
    avatar: Optional[str] = None
    team_id: Optional[str] = None
    bio: Optional[str] = None
    site_url: Optional[str] = None
    type: Optional[str] = None
    geolocation_id: Optional[str] = None

class Crop(BaseModel):
    # key: Optional[str] = None
    crop: str
    description: Optional[str] = None

class Trait(BaseModel):
    # key: Optional[str] = None
    title: str
    content: Optional[str] = None
    type: Optional[str] = None
    options: Optional[str] = None
    crop_cycle: Optional[str] = None
    lang: Optional[str] = None

class FbForm(BaseModel):
    # key: Optional[str] = None
    crop: str
    form: str
    obj: Optional[str] = None
    title: str
    managment: str
    project_id: Optional[int] = None

class Project(BaseModel):
    id: Optional[int] = None
    uuid: Optional[str] = None
    name: str
    created_date: Optional[str] = None
    team_id: Optional[str] = None
    organization_id: Optional[str] = None
    managment: Optional[str] = None
    description: Optional[str] = None
    traits_advances: Optional[bool] = False
    traits_basics: Optional[bool] = False

class FbFormItem(BaseModel):
    id: Optional[int] = None
    name: str
    title: Optional[str] = None
    alias: Optional[str] = None
    description: Optional[str] = None
    text_label: Optional[str] = None
    type_format: Optional[str] = None
    fieldbook_id: Optional[int] = None
    category_format: Optional[str] = None 
    optional: Optional[bool] = None
    enable: Optional[bool] = None
    options: Optional[str] = None 
    crop_cycle: Optional[str] = None 
    form_order: Optional[int] = None

class Trail(BaseModel):
    id: Optional[int] = None
    uuid: Optional[str] = None
    created_date: Optional[str] = None
    creator_id: int
    fieldbook_id: int
    name: Optional[str] = None

class TrailEnvironment(BaseModel):
    id: Optional[int] = None
    trail_id: int
    value: str    

class TrailSeeds(BaseModel):
    id: Optional[int] = None
    trail_id: int
    seed_id: int

class TrailFieldbookData(BaseModel):
    id: Optional[int] = None
    trail_id: int
    item_id: int
    created_date: Optional[str] = None
    date_observation: Optional[str] = None
    creator_id: int
    trailseeds_id: int
    value: str

class Topic(BaseModel):
    id: Optional[int] = None
    title: str
    created_date: Optional[str] = None
    created_user_id: Optional[int] = None
    visits: Optional[int] = None
    content: Optional[str] = None
    category: Optional[str] = None

class Reply(BaseModel):
    id: Optional[int] = None
    topic_id: Optional[int] = None
    content: Optional[str] = None
    created_date: Optional[str] = None
    created_user_id: Optional[int] = None

class Community(BaseModel):
    search: Optional[str] = None
    people: List[dict]
    orgs: List[dict]
    species: Optional[str] = None
    activities: Optional[str] = None
    products_offered: Optional[str] = None
