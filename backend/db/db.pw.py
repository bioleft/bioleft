from peewee import *
from config import Config

db = PostgresqlDatabase(
    Config.POSTGRES_NAME, 
    user=Config.POSTGRES_USER, 
    host=Config.POSTGRES_HOST, 
    password=Config.POSTGRES_PASSWORD
)

if Config.DATABASE == 'sqlite':
    db = SqliteDatabase('sqlbioleft.db')

class ModelBase(Model):
    class Meta:
        database = db
    