from bioleft.models.seed import *
from playhouse.shortcuts import dict_to_model

list_licences = [
    {
        'name': 'Multiplicacion abierta',
        'detail': 'Se permite el uso para investigación, \
                    desarrollo y registro de nuevas variedades.<br> \
                    Se permite el guardado de la semilla para uso propio.<br> \
                    Se permite la multiplicación y venta, donación o  \
                    intercambio posterior de la semilla tal como es, sin mejoras genéticas.',
        'image':'licence/ma.png',
    },
    {
        'name': 'Multiplicacion exclusiva',
        'detail': 'Se permite el uso para investigación, \
                    desarrollo y registro de nuevas variedades.<br> \
                    Se permite el guardado de la semilla para uso propio.<br> \
                    La multiplicación para venta/ donación/ intercambio es \
                    posible sólo bajo autorización expresa del proveedor.',
        'image':'licence/me.png',
    },
    {
        'name': 'Sin multiplicacion',
        'detail': 'Se permite el uso para investigación, \
                    desarrollo y registro de nuevas variedades.<br>\
                    Se permite el guardado de la semilla para uso propio.<br> \
                    No se permite multiplicar la semilla.',
        'image':'licence/sm.png',
    },
]

list_species = [
    {
        'name': 'acelga',
        'image': '',
    },
    {
        'name': 'achicoria',
        'image': '',
    },
    {
        'name': 'adzuki',
        'image': '',
    },
    {
        'name': 'agropiro alargado',
        'image': '',
    },
    {
        'name': 'agropiro criollo',
        'image': '',
    },
    {
        'name': 'agropyron cristatum',
        'image': '',
    },
    {
        'name': 'agropyron desertorum',
        'image': '',
    },
    {
        'name': 'agropyron intermedium',
        'image': '',
    },
    {
        'name': 'agropyron trichoforum',
        'image': '',
    },
    {
        'name': 'agrostis',
        'image': '',
    },
    {
        'name': 'aji',
        'image': '',
    },
    {
        'name': 'ajo',
        'image': '',
    },
    {
        'name': 'ajo elefante',
        'image': '',
    },
    {
        'name': 'alamo',
        'image': '',
    },
    {
        'name': 'albahaca',
        'image': '',
    },
    {
        'name': 'alcaparra',
        'image': '',
    },
    {
        'name': 'alcaucil',
        'image': '',
    },
    {
        'name': 'alfalfa',
        'image': '',
    },
    {
        'name': 'algodonero',
        'image': '',
    },
    {
        'name': 'almendro',
        'image': '',
    },
    {
        'name': 'alpiste',
        'image': '',
    },
    {
        'name': 'alstroemeria',
        'image': '',
    },
    {
        'name': 'amaranto',
        'image': '',
    },
    {
        'name': 'apio',
        'image': '',
    },
    {
        'name': 'arandano',
        'image': '',
    },
    {
        'name': 'arroz',
        'image': '',
    },
    {
        'name': 'arveja',
        'image': '',
    },
    {
        'name': 'avellano',
        'image': '',
    },
    {
        'name': 'avena',
        'image': '',
    },
    {
        'name': 'avena amarilla',
        'image': '',
    },
    {
        'name': 'avena blanca',
        'image': '',
    },
    {
        'name': 'avena forrajera',
        'image': '',
    },
    {
        'name': 'avena sativa',
        'image': '',
    },
    {
        'name': 'avena strigosa',
        'image': '',
    },
    {
        'name': 'avena strigosa schreber',
        'image': '',
    },
    {
        'name': 'azucena',
        'image': '',
    },
    {
        'name': 'batata',
        'image': '',
    },
    {
        'name': 'berenjena',
        'image': '',
    },
    {
        'name': 'bergamota',
        'image': '',
    },
    {
        'name': 'boton de oro',
        'image': '',
    },
    {
        'name': 'brachiaria',
        'image': '',
    },
    {
        'name': 'brachiaria brizantha',
        'image': '',
    },
    {
        'name': 'brachiaria hibrida',
        'image': '',
    },
    {
        'name': 'brassica forrajera rape',
        'image': '',
    },
    {
        'name': 'brocoli',
        'image': '',
    },
    {
        'name': 'bromus de las praderas',
        'image': '',
    },
    {
        'name': 'bromus parodi',
        'image': '',
    },
    {
        'name': 'bromus perenne',
        'image': '',
    },
    {
        'name': 'buffel grass',
        'image': '',
    },
    {
        'name': 'calabaza',
        'image': '',
    },
    {
        'name': 'calabaza mixta',
        'image': '',
    },
    {
        'name': 'calibrachoa',
        'image': '',
    },
    {
        'name': 'camelina sativa',
        'image': '',
    },
    {
        'name': 'canola',
        'image': '',
    },
    {
        'name': 'caña de azucar',
        'image': '',
    },
    {
        'name': 'cartamo',
        'image': '',
    },
    {
        'name': 'caupi',
        'image': '',
    },
    {
        'name': 'cebada cervecera',
        'image': '',
    },
    {
        'name': 'cebada forrajera',
        'image': '',
    },
    {
        'name': 'cebadilla',
        'image': '',
    },
    {
        'name': 'cebadilla australiana',
        'image': '',
    },
    {
        'name': 'cebadilla chaqueña',
        'image': '',
    },
    {
        'name': 'cebadilla criolla',
        'image': '',
    },
    {
        'name': 'cebadilla de hungria o perenne',
        'image': '',
    },
    {
        'name': 'cebadilla intermedia',
        'image': '',
    },
    {
        'name': 'cebadilla pampeana',
        'image': '',
    },
    {
        'name': 'cebadilla perenne',
        'image': '',
    },
    {
        'name': 'cebolla',
        'image': '',
    },
    {
        'name': 'cebolla de verdeo',
        'image': '',
    },
    {
        'name': 'centeno',
        'image': '',
    },
    {
        'name': 'cerezo',
        'image': '',
    },
    {
        'name': 'cesped',
        'image': '',
    },
    {
        'name': 'chalota',
        'image': '',
    },
    {
        'name': 'chia',
        'image': '',
    },
    {
        'name': 'chuscho del monte',
        'image': '',
    },
    {
        'name': 'ciruela adamascada',
        'image': '',
    },
    {
        'name': 'ciruelo europeo',
        'image': '',
    },
    {
        'name': 'ciruelo interespecifico',
        'image': '',
    },
    {
        'name': 'ciruelo japones',
        'image': '',
    },
    {
        'name': 'citrandarin (c.retic. x p. trifol.)',
        'image': '',
    },
    {
        'name': 'citrange (c. sinensis x p. trifol)',
        'image': '',
    },
    {
        'name': 'citrumelo (c.paradisi x p. trifol)',
        'image': '',
    },
    {
        'name': 'citrus (otros hibridos)',
        'image': '',
    },
    {
        'name': 'citrus sp.',
        'image': '',
    },
    {
        'name': 'col china',
        'image': '',
    },
    {
        'name': 'col de bruselas',
        'image': '',
    },
    {
        'name': 'coliflor',
        'image': '',
    },
    {
        'name': 'colza-canola',
        'image': '',
    },
    {
        'name': 'comino',
        'image': '',
    },
    {
        'name': 'coriandro',
        'image': '',
    },
    {
        'name': 'damasco',
        'image': '',
    },
    {
        'name': 'desmanthus',
        'image': '',
    },
    {
        'name': 'dicantio',
        'image': '',
    },
    {
        'name': 'digitaria',
        'image': '',
    },
    {
        'name': 'dolichos',
        'image': '',
    },
    {
        'name': 'dumosa / yerba señorita',
        'image': '',
    },
    {
        'name': 'duraznero',
        'image': '',
    },
    {
        'name': 'eragrostis robusta',
        'image': '',
    },
    {
        'name': 'escarola',
        'image': '',
    },
    {
        'name': 'esparrago',
        'image': '',
    },
    {
        'name': 'espinaca',
        'image': '',
    },
    {
        'name': 'estevia',
        'image': '',
    },
    {
        'name': 'eucalipto',
        'image': '',
    },
    {
        'name': 'festuca',
        'image': '',
    },
    {
        'name': 'festuca alta',
        'image': '',
    },
    {
        'name': 'festuca rubra',
        'image': '',
    },
    {
        'name': 'festulolium',
        'image': '',
    },
    {
        'name': 'ficus',
        'image': '',
    },
    {
        'name': 'fotinia',
        'image': '',
    },
    {
        'name': 'frambuesa',
        'image': '',
    },
    {
        'name': 'fromental',
        'image': '',
    },
    {
        'name': 'frutilla',
        'image': '',
    },
    {
        'name': 'garbanzo',
        'image': '',
    },
    {
        'name': 'girasol',
        'image': '',
    },
    {
        'name': 'girasol confitura',
        'image': '',
    },
    {
        'name': 'girasol hibrido con oficio judicial',
        'image': '',
    },
    {
        'name': 'girasol ornamental',
        'image': '',
    },
    {
        'name': 'glandularia',
        'image': '',
    },
    {
        'name': 'grama de rhodes',
        'image': '',
    },
    {
        'name': 'granada',
        'image': '',
    },
    {
        'name': 'guaran',
        'image': '',
    },
    {
        'name': 'gypsophila',
        'image': '',
    },
    {
        'name': 'haba',
        'image': '',
    },
    {
        'name': 'hibrido de sauce',
        'image': '',
    },
    {
        'name': 'hinojo',
        'image': '',
    },
    {
        'name': 'hongo endofito',
        'image': '',
    },
    {
        'name': 'incayuyo',
        'image': '',
    },
    {
        'name': 'kale',
        'image': '',
    },
    {
        'name': 'kiwi',
        'image': '',
    },
    {
        'name': 'kiwi amarillo',
        'image': '',
    },
    {
        'name': 'kiwi arguta',
        'image': '',
    },
    {
        'name': 'kleingrass',
        'image': '',
    },
    {
        'name': 'kumquat',
        'image': '',
    },
    {
        'name': 'lapacho negro',
        'image': '',
    },
    {
        'name': 'lechuga',
        'image': '',
    },
    {
        'name': 'lenteja',
        'image': '',
    },
    {
        'name': 'lespedeza',
        'image': '',
    },
    {
        'name': 'leucaena',
        'image': '',
    },
    {
        'name': 'lima acida de fruto grande',
        'image': '',
    },
    {
        'name': 'lima acida de fruto pequeño',
        'image': '',
    },
    {
        'name': 'lima dulce',
        'image': '',
    },
    {
        'name': 'limonero',
        'image': '',
    },
    {
        'name': 'lino',
        'image': '',
    },
    {
        'name': 'lolium perenne',
        'image': '',
    },
    {
        'name': 'lotus corniculatus',
        'image': '',
    },
    {
        'name': 'lotus glaber',
        'image': '',
    },
    {
        'name': 'lotus subbiflorus',
        'image': '',
    },
    {
        'name': 'lotus tenuis',
        'image': '',
    },
    {
        'name': 'lotus uliginosus',
        'image': '',
    },
    {
        'name': 'lupino azul (altramuz azul)',
        'image': '',
    },
    {
        'name': 'lupulo',
        'image': '',
    },
    {
        'name': 'macroptilium erythroloma(mar.et ben',
        'image': '',
    },
    {
        'name': 'maiz',
        'image': '',
    },
    {
        'name': 'maiz superdulce',
        'image': '',
    },
    {
        'name': 'makarikari grass',
        'image': '',
    },
    {
        'name': 'mandarino',
        'image': '',
    },
    {
        'name': 'mandioca',
        'image': '',
    },
    {
        'name': 'mango',
        'image': '',
    },
    {
        'name': 'mani',
        'image': '',
    },
    {
        'name': 'manzanilla',
        'image': '',
    },
    {
        'name': 'manzano',
        'image': '',
    },
    {
        'name': 'mecardonia',
        'image': '',
    },
    {
        'name': 'medicago littoralis',
        'image': '',
    },
    {
        'name': 'medicago polymorpha',
        'image': '',
    },
    {
        'name': 'medicago scutellata',
        'image': '',
    },
    {
        'name': 'medicago truncatula',
        'image': '',
    },
    {
        'name': 'melilotus',
        'image': '',
    },
    {
        'name': 'melilotus amarillo',
        'image': '',
    },
    {
        'name': 'melon',
        'image': '',
    },
    {
        'name': 'membrillero',
        'image': '',
    },
    {
        'name': 'mijo',
        'image': '',
    },
    {
        'name': 'mijo perla',
        'image': '',
    },
    {
        'name': 'mimbre',
        'image': '',
    },
    {
        'name': 'mimbre japones',
        'image': '',
    },
    {
        'name': 'moha',
        'image': '',
    },
    {
        'name': 'mostaza',
        'image': '',
    },
    {
        'name': 'mostaza etiope',
        'image': '',
    },
    {
        'name': 'murtilla',
        'image': '',
    },
    {
        'name': 'nabo',
        'image': '',
    },
    {
        'name': 'nabo forrajero',
        'image': '',
    },
    {
        'name': 'naranja dulce',
        'image': '',
    },
    {
        'name': 'naranjo',
        'image': '',
    },
    {
        'name': 'naranjo acido',
        'image': '',
    },
    {
        'name': 'naranjo dulce',
        'image': '',
    },
    {
        'name': 'nectarina',
        'image': '',
    },
    {
        'name': 'nogal',
        'image': '',
    },
    {
        'name': 'ocra',
        'image': '',
    },
    {
        'name': 'olivo',
        'image': '',
    },
    {
        'name': 'oregano',
        'image': '',
    },
    {
        'name': 'pak choi',
        'image': '',
    },
    {
        'name': 'palta',
        'image': '',
    },
    {
        'name': 'panicum maximum',
        'image': '',
    },
    {
        'name': 'papa',
        'image': '',
    },
    {
        'name': 'papaya',
        'image': '',
    },
    {
        'name': 'paspalum atratum swallen',
        'image': '',
    },
    {
        'name': 'pasto de guinea',
        'image': '',
    },
    {
        'name': 'pasto horqueta',
        'image': '',
    },
    {
        'name': 'pasto lloron',
        'image': '',
    },
    {
        'name': 'pasto makarikari',
        'image': '',
    },
    {
        'name': 'pasto miel',
        'image': '',
    },
    {
        'name': 'pasto ovillo',
        'image': '',
    },
    {
        'name': 'pasto ramirez',
        'image': '',
    },
    {
        'name': 'pasto rojas',
        'image': '',
    },
    {
        'name': 'pasto romano',
        'image': '',
    },
    {
        'name': 'pecan',
        'image': '',
    },
    {
        'name': 'pennisetum',
        'image': '',
    },
    {
        'name': 'peperina',
        'image': '',
    },
    {
        'name': 'pepino',
        'image': '',
    },
    {
        'name': 'peral',
        'image': '',
    },
    {
        'name': 'perejil',
        'image': '',
    },
    {
        'name': 'petunia',
        'image': '',
    },
    {
        'name': 'phalaris angusta nees.',
        'image': '',
    },
    {
        'name': 'phalaris aquatica l.',
        'image': '',
    },
    {
        'name': 'phalaris bulbosa',
        'image': '',
    },
    {
        'name': 'phalaris tuberinacea',
        'image': '',
    },
    {
        'name': 'pimiento',
        'image': '',
    },
    {
        'name': 'pimpinela',
        'image': '',
    },
    {
        'name': 'pistacho',
        'image': '',
    },
    {
        'name': 'plumerillo',
        'image': '',
    },
    {
        'name': 'plumerito',
        'image': '',
    },
    {
        'name': 'poa',
        'image': '',
    },
    {
        'name': 'poa cesped',
        'image': '',
    },
    {
        'name': 'poa de los prados',
        'image': '',
    },
    {
        'name': 'poa hibrida cesped',
        'image': '',
    },
    {
        'name': 'poa pratensis',
        'image': '',
    },
    {
        'name': 'poa.',
        'image': '',
    },
    {
        'name': 'pomelo',
        'image': '',
    },
    {
        'name': 'poroto',
        'image': '',
    },
    {
        'name': 'poroto chaucha',
        'image': '',
    },
    {
        'name': 'poroto de campo',
        'image': '',
    },
    {
        'name': 'poroto mung',
        'image': '',
    },
    {
        'name': 'portainjerto',
        'image': '',
    },
    {
        'name': 'portainjerto cerezo',
        'image': '',
    },
    {
        'name': 'portainjerto ciruelo',
        'image': '',
    },
    {
        'name': 'portainjerto dadiviana x duraznero',
        'image': '',
    },
    {
        'name': 'portainjerto de berenjena',
        'image': '',
    },
    {
        'name': 'portainjerto de manzano',
        'image': '',
    },
    {
        'name': 'portainjerto de prunus',
        'image': '',
    },
    {
        'name': 'portainjerto de tomate interesp.',
        'image': '',
    },
    {
        'name': 'portainjerto duraznero',
        'image': '',
    },
    {
        'name': 'portainjerto duraznero x almendro',
        'image': '',
    },
    {
        'name': 'portainjerto interespecifico',
        'image': '',
    },
    {
        'name': 'portainjerto manzano',
        'image': '',
    },
    {
        'name': 'portainjerto pera',
        'image': '',
    },
    {
        'name': 'puerro',
        'image': '',
    },
    {
        'name': 'quinoa',
        'image': '',
    },
    {
        'name': 'rabanito',
        'image': '',
    },
    {
        'name': 'rabano',
        'image': '',
    },
    {
        'name': 'raigras anual',
        'image': '',
    },
    {
        'name': 'raigras anual cesped',
        'image': '',
    },
    {
        'name': 'raigras hibrido',
        'image': '',
    },
    {
        'name': 'raigras italiano',
        'image': '',
    },
    {
        'name': 'raigras per. cesped',
        'image': '',
    },
    {
        'name': 'raigras perenne',
        'image': '',
    },
    {
        'name': 'raigras perenne cesped',
        'image': '',
    },
    {
        'name': 'remolacha',
        'image': '',
    },
    {
        'name': 'repollo',
        'image': '',
    },
    {
        'name': 'repollo chino',
        'image': '',
    },
    {
        'name': 'repollo de bruselas',
        'image': '',
    },
    {
        'name': 'rosa',
        'image': '',
    },
    {
        'name': 'rucula',
        'image': '',
    },
    {
        'name': 'salvia',
        'image': '',
    },
    {
        'name': 'sandia',
        'image': '',
    },
    {
        'name': 'satsuma',
        'image': '',
    },
    {
        'name': 'sauce',
        'image': '',
    },
    {
        'name': 'sauce americano',
        'image': '',
    },
    {
        'name': 'sericea lespedeza',
        'image': '',
    },
    {
        'name': 'sesamo',
        'image': '',
    },
    {
        'name': 'setaria',
        'image': '',
    },
    {
        'name': 'siratro',
        'image': '',
    },
    {
        'name': 'soja',
        'image': '',
    },
    {
        'name': 'soja forrajera',
        'image': '',
    },
    {
        'name': 'soja perenne',
        'image': '',
    },
    {
        'name': 'sorgo',
        'image': '',
    },
    {
        'name': 'sorgo azucarado silero',
        'image': '',
    },
    {
        'name': 'sorgo de escoba',
        'image': '',
    },
    {
        'name': 'sorgo silero',
        'image': '',
    },
    {
        'name': 'suico',
        'image': '',
    },
    {
        'name': 'tabaco',
        'image': '',
    },
    {
        'name': 'tangelo',
        'image': '',
    },
    {
        'name': 'tangor',
        'image': '',
    },
    {
        'name': 'tangor (c.reticulata x c. sinensis)',
        'image': '',
    },
    {
        'name': 'tartago',
        'image': '',
    },
    {
        'name': 'te',
        'image': '',
    },
    {
        'name': 'tomate',
        'image': '',
    },
    {
        'name': 'trebol blanco',
        'image': '',
    },
    {
        'name': 'trebol caucasico',
        'image': '',
    },
    {
        'name': 'trebol de alejandria',
        'image': '',
    },
    {
        'name': 'trebol de los cuernitos',
        'image': '',
    },
    {
        'name': 'trebol de olor blanco',
        'image': '',
    },
    {
        'name': 'trebol encarnado',
        'image': '',
    },
    {
        'name': 'trebol frutilla',
        'image': '',
    },
    {
        'name': 'trebol persa',
        'image': '',
    },
    {
        'name': 'trebol rojo',
        'image': '',
    },
    {
        'name': 'tricepiro',
        'image': '',
    },
    {
        'name': 'trifolio',
        'image': '',
    },
    {
        'name': 'trigo blando-galletitero o soft',
        'image': '',
    },
    {
        'name': 'trigo espelta',
        'image': '',
    },
    {
        'name': 'trigo fideos o candeal',
        'image': '',
    },
    {
        'name': 'trigo fideos-prohib. de contratar',
        'image': '',
    },
    {
        'name': 'trigo pan',
        'image': '',
    },
    {
        'name': 'trigo pan-prohibicion de contratar',
        'image': '',
    },
    {
        'name': 'trigo sarraceno',
        'image': '',
    },
    {
        'name': 'trigopiro',
        'image': '',
    },
    {
        'name': 'triticale',
        'image': '',
    },
    {
        'name': 'triticale-prohibicion de contratar',
        'image': '',
    },
    {
        'name': 'vicia benghalensis',
        'image': '',
    },
    {
        'name': 'vicia dasycarpa ten.',
        'image': '',
    },
    {
        'name': 'vicia faba l.',
        'image': '',
    },
    {
        'name': 'vicia pannonica',
        'image': '',
    },
    {
        'name': 'vicia sativa l.',
        'image': '',
    },
    {
        'name': 'vicia villosa roth',
        'image': '',
    },
    {
        'name': 'vid',
        'image': '',
    },
    {
        'name': 'yerba mate',
        'image': '',
    },
    {
        'name': 'zanahoria',
        'image': '',
    },
    {
        'name': 'zapallito',
        'image': '',
    },
    {
        'name': 'zapallito alargado de tronco',
        'image': '',
    },
    {
        'name': 'zapallito redondo de tronco',
        'image': '',
    },
    {
        'name': 'zapallo',
        'image': '',
    },
    {
        'name': 'zapallo pie de injerto',
        'image': '',
    },
    {
        'name': 'zapallo/zapallito',
        'image': '',
    },
    {
        'name': 'zapallo/zapallito interespecif',
        'image': '',
    },
    {
        'name': 'zapallo/zapallito redondo linea',
        'image': '',
    },
    {
        'name': 'zarzamora',
        'image': '',
    },
    {
        'name': 'zoysiagrass',
        'image': '',
    },
    {
        'name': 'zuchini',
        'image': '',
    }
]


attributes = [
    {
        'name': 'area de uso',
        'description': 'Localidad donde se desarrolló el material y/o donde funciona bien el mismo',
    },
    {
        'name': 'tipo de tomate',
        'options': 'cherry,perita,redondo,criollo (surcado)'
    },
    {
        'name': 'tipo de crecimiento',
        'options': 'determinado,indeterminado '
    },
    {
        'name': 'tamaño fruto',
        'description': 'peso promedio de frutos',
        'options': 'determinado,indeterminado'
    },
    {
        'name': 'tamaño fruto',
        'description': 'alternativa a la anterior',
        'options': 'chico,mediano,grande,muy grande'
    },
    {
        'name': 'ciclo',
        'options': 'precoz,intermedio,tardío'
    },
    {
        'name': 'manejo',
        'options': 'sin uso de agroquímicos de síntesis, con uso de agroquímicos de síntesis'
    },
    {
        'name': 'condición ambiental donde se evaluó el rendimiento',
        'group': 'rendimiento',
    },
    {
        'name': 'rendimiento promedio (qq/ha)',
        'group': 'rendimiento',
    },
    {
        'name': 'modificación génica'
    },
    {
        'name': 'tipo de material',
        'options': 'híbrido, no híbrido',
        'group': 'descriptores'
    },
    {
        'name': 'tipo de grano',
        'options': 'semidentado,dentado,duro,flint,harinoso,dulce,super dulce,pisingallo,cuarentín',
        'group': 'descriptores'
    },
    {
        'name': 'color de grano',
        'options': 'colorado,amarillo,blanco,morado,variegado',
        'group': 'descriptores'
    },
    {
        'name': 'GDU emergencia a floración',
        'description': 'suma térmica, por encima de los 10 °C',
        'group': 'descriptores'
    },
    {
        'name': 'duración del ciclo',
        'description': 'de siembra a cosecha',
        'options': 'corto,intermedio corto,intermedio,intermedio largo,largo',
        'group': 'descriptores'
    },
    {
        'name': 'PMS (g)',
        'description': 'peso de mil semillas',
        'group': 'descriptores'
    },
    {
        'name': 'altura planta',
        'group': 'descriptores'
    },
    {
        'name': 'altura espiga',
        'group': 'descriptores'
    },
    {
        'name': 'color de fruto'
    },
]


attribute_specie = [
    {
        'attribute': [{'name': 'area de uso'}],
        'each_specie': True,
    },{
        'attribute': [
            {'name': 'area de uso'},
            {'name': 'tipo de tomate'},
            {'name': 'tipo de crecimiento'},
            {'name': 'tamaño fruto'},
            {'name': 'tamaño fruto'},
            {'name': 'ciclo'},
            {'name': 'color de fruto'},
        ],
        'species': 'tomate',
    },{
        'attribute': [
            {'name': 'manejo'},
            {'name': 'condición ambiental donde se evaluó el rendimiento'},
            {'name': 'rendimiento promedio (qq/ha)'},
            {'name': 'modificación génica'},
            {'name': 'tipo de material'},
            {'name': 'tipo de grano'},
            {'name': 'color de grano'},
            {'name': 'GDU emergencia a floración'},
            {'name': 'duración del ciclo'},
            {'name': 'PMS (g)'},
            {'name': 'altura planta'},
            {'name': 'altura espiga'},
        ],
        'species': 'maiz',
    }
]

def bioleft_config():

    for s in list_species:
        if not Species.select().where(Species.name==s['name']).count():
            seed_species = dict_to_model(Species, s)
            seed_species.save()

    for attribute in attributes:
        if not SeedAttributes.select().where(SeedAttributes.name==attribute['name']):
            seed_attribute = dict_to_model(SeedAttributes, attribute)
            seed_attribute.save()

    for sp_attr in attribute_specie:
        for attrinsp in sp_attr['attribute']:
            attribute = SeedAttributes.get(SeedAttributes.name==attrinsp['name'])
            if 'each_specie' in sp_attr and sp_attr['each_specie']:
                for species in Species.select():
                    if not SeedAttributeSpecies.select().where(SeedAttributeSpecies.species==species, SeedAttributeSpecies.attribute==attribute):
                        SeedAttributeSpecies.create(
                            species = species,
                            attribute = attribute
                        )
            else:
                species = Species.get(Species.name==sp_attr['species'])
                if not SeedAttributeSpecies.select().where(SeedAttributeSpecies.species==species, SeedAttributeSpecies.attribute==attribute):
                    SeedAttributeSpecies.create(
                        species = species,
                        attribute = attribute
                    )

    for licence in list_licences:
        if not Licence.select().where(Licence.name==['name']):
            Licence.create(
                name=licence['name'],
                detail=licence['detail'],
                image=licence['image']
            )