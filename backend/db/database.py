import os
from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from aioredis import Redis, from_url
import models

load_dotenv()

# SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
SQLALCHEMY_DATABASE_URL = "postgresql://%s:%s@%s/%s" % (
    os.getenv("POSTGRES_USER"),
    os.getenv("POSTGRES_PASSWORD"),
    os.getenv("POSTGRES_HOST"),
    os.getenv("POSTGRES_NAME")
)

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

async def conn_redis(db) -> Redis:
    redis = await from_url(
        os.environ.get("REDIS_URL", "redis://localhost:6379/%s"%db),
        encoding="utf-8",
        decode_responses=True,
    )
    return redis

class StoreRedis:
    def __init__(self, db: int = 0):
        self.db = db

    async def conn(self):
        return await conn_redis(self.db)
        

Base = declarative_base()
# metadata = Base.metadata
