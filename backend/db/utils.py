from bioleft.models import *
from playhouse.migrate import *
from .db import db

my_db = SqliteDatabase('sqlite:///:memory:')
migrator = SqliteMigrator(my_db)

def run_migrate():
    
    db.create_tables([
        user.User
    ])

    resutls = migrate(
        migrator.add_column('user', 'username', user.User.username),
        # migrator.add_column('some_table', 'status', status_field),
        # migrator.drop_column('some_table', 'old_column'),
    )
    print(results)
    return results