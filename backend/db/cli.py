import click
from flask.cli import with_appcontext
from flask.cli import AppGroup
from bioleft.db import db
from bioleft.db.seed import *
from bioleft.models.user import User

db_cli = AppGroup('db')

@db_cli.command('config')
def config():
    bioleft_config()

@db_cli.command('createsuperuser')
@click.argument('email')
@click.argument('password')
def superuser(email, password):
    if not User.get_or_none(email=email):
        user = User.create(
            email=email,
            password=password,
            admin=True,
            username=email.replace('@','').replace('.','')
        )
        print('%s creado con exito!' % user)
