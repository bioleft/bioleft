import hmac
import os
import hashlib
import base64
import datetime
from datetime import datetime, timedelta
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel
from typing import Optional, List, Any
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from db.database import get_db
from utils.geo import service
import models, schemas

SECRET_KEY = "96179773ef00890fba9ce086f88b63ec33dadbff633de70e9d63fb7462556507"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 1

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/user/token")

pwd_context = CryptContext(schemes=["bcrypt", "sha256_crypt", "sha512_crypt"], deprecated="auto")

folder_static_avatar = os.getenv('STATIC_FOLDER')+'/content/avatars/'

def encode_string(string):
    """Encodes a string to bytes, if it isn't already.
    :param string: The string to encode"""

    if isinstance(string, str):
        string = string.encode("utf-8")
    return string

def get_hmac(password) -> bytes:
    """Returns a Base64 encoded HMAC+SHA512 of the password signed with
    the salt specified by *SECURITY_PASSWORD_SALT*.
    :param password: The password to sign
    """
    salt = "235069836344249260092709921577927120663"
    h = hmac.new(encode_string(salt), encode_string(password), hashlib.sha512)
    return base64.b64encode(h.digest())

def verify_password(plain_password, password):
    plain_password = get_hmac(plain_password)
    return pwd_context.verify(plain_password, password)

def get_password_hash(password):
    return pwd_context.hash(password)

def get_user(db, username: str) -> schemas.UserInDB:
    usr = db.query(models.User).filter(models.User.email == username).first()
    return usr

def authenticate_user(db, username: str, password: str):
    user = get_user(db, username)
    if not user:
        return False
    if user.updated_date == None:
        user.password = get_password_hash(get_hmac(password))
        user.updated_date = datetime.now()
        db.commit()
        db.refresh(user)
    if password != user.password and not verify_password(password, user.password):
        return False
    return user

def registry_user(db: Session, user: schemas.UserInDB):
    user.password = get_password_hash(get_hmac(user.password))
    db_item = models.User(**user.dict())
    db_item.avatar = "default.png"
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

async def get_current_user(
    db: Session = Depends(get_db),
    token: str = Depends(oauth2_scheme)
)-> schemas.User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user

async def get_current_active_user(
    current_user: schemas.User = Depends(get_current_user)
):
    return current_user

def config_password(
    db: Session,
    user: models.User,
    password: str
):
    user.password = get_password_hash(get_hmac(password))
    db.commit()
    db.refresh(user)
    return user

def config_user(
    db: Session,
    user: models.User
):
    db_item = db.query(models.User).filter(
        models.User.email==user.email
    ).first()

    if user.geolocation != None:
        args = dict(reg="ar",loc=str.title(user.geolocation.location),limit=5)

        if 'De' in args['loc'] and args['loc'].index('De') != 0:
            args['loc'] = args['loc'].replace('De', 'de')

        geolocation = service.georef(args)

        if(geolocation):
            db_model_geo = models.Geolocation(
                display_name = geolocation[0]['nombre'],
                location = geolocation[0]['nombre'],
                latitude = str(geolocation[0]['centroide']['lat']),
                longitude = str(geolocation[0]['centroide']['lon']),
            )
            db_item_geo = db.query(models.Geolocation).filter(
                models.Geolocation.location == db_model_geo.location,
                models.Geolocation.display_name == db_model_geo.display_name,
                models.Geolocation.latitude == db_model_geo.latitude,
                models.Geolocation.longitude == db_model_geo.longitude,
            )
            if(db_item_geo.count() == 0):
                db.add(db_model_geo)
                db.commit()
                db.refresh(db_model_geo)
            else:
                db_model_geo = db_item_geo.first()

            user.geolocation = db_model_geo

    for key, value in user:
        setattr(db_item, key, value)

    db.commit()
    db.refresh(db_item)
    db_item.geolocation

    return db_item

def get_account_requests(db: Session, user: models.User):
    db_user_request = db.query(models.Seedtransferrequest).filter(
        models.Seedtransferrequest.user_request == user
    ).all()
    for request_ in db_user_request:
        request_.seed
    return db_user_request
