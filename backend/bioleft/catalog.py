import uuid
from typing import List
from fastapi import Depends
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql.expression import func, select
from sqlalchemy.orm import lazyload
import models, schemas

class Catalog():

    def seeds(self, db: Session, filters):
        db_items = db.query(models.Seed).filter(
            models.Seed.owner_id==models.Seed.creator_id
        ).join(models.Seedtransferconfig)

        if filters.licence:
            db_items = db_items.filter(
                models.Seed.licence_id==filters.licence
            )

        if filters.type:
            db_items = db_items.filter(
                models.Seed.type_==filters.type
            )

        if filters.cultivar:
            db_items = db_items.filter(
                models.Seed.cultivar.ilike(f'%{filters.cultivar}%')
            )

        if filters.species:
            species = filters.species.split(',')
            query_species = []
            for specie in species:
                db_specie = db.query(models.Species).filter(
                    models.Species.name.ilike(f'%{specie.lower()}%')
                ).first()
                query_species.append(db_specie.id)
            db_items = db_items.join(models.Species).filter(
                models.Species.id.in_(query_species)
            )

        if filters.location:
            db_locs = db.query(models.Geolocation).filter(
                models.Geolocation.location == filters.location,
            ).first()
            db_items = db_items.filter(
                models.Seedtransferconfig.geolocation==db_locs
            )

        if filters.order:
            if filters.order == "cultivar":
                db_items = db_items.order_by(models.Seed.cultivar)
            if filters.order == "desc":
                db_items = db_items.order_by(models.Seed.created_date.desc())
            if filters.order == "asc":
                db_items = db_items.order_by(models.Seed.created_date.asc())
        else:
            db_items = db_items.order_by(func.random())

        db_results = []
        for db_seed in db_items.all():
            if len(db_seed.offer_config)>0:
                db_seed.owner
                db_seed.licence
                db_seed.species
                if db_seed.attributes:
                    db_seed.attributes
                    for attr in db_seed.attributes:
                        attr.attribute
                db_results.append(db_seed)
        return db_results

    def get_from_user(
        self, 
        db: Session, 
        user: schemas.User
    ):
        seeds = db.query(models.Seed).filter(
            models.Seed.owner_id==user.id
        ).all()
        results = []
        for seed in seeds:
            seed.owner
            seed.licence
            seed.species
            results.append(seed)
        return results

    def get_by_uuid(
        self, 
        db: Session, 
        uuid: str
    ):
        seed = db.query(models.Seed).filter(
            models.Seed.uuid==uuid
        ).first()
        seed.owner
        seed.licence
        seed.species
        seed.organization
        seed.attributes
        for attr in seed.attributes:
            attr.attribute
        return seed

    def registry(
        self, 
        db: Session, 
        account: str, 
        seed: str
    ):
        user = db.query(models.User).filter(
            models.User.username == account
        ).first()
        qseed = db.query(models.Seed).filter(
            models.Seed.uuid == seed,
            models.Seed.owner_id == user.id
        ).first()
        qseed.owner
        qseed.licence
        qseed.species
        return qseed

catalog = Catalog()
