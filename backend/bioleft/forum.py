from sqlalchemy.orm import Session
import models, schemas

class Forum():

    def topic_get(
        self, 
        db: Session, 
        topic: int
    ):
        db_item = db.query(models.Topic).get(topic)
        if(db_item):
            db_item.replies
            for r in db_item.replies:
                r.created_user
            return db_item
        return {}

    def topics_category(
        self, 
        db: Session, 
        category: str
    ):
        db_items = db.query(models.Topic).filter(
            models.Topic.category==category
        ).all()
        for dbi in db_items:
            dbi.created_user
            dbi.replies
            dbi.last_reply = dbi.replies[-1] if dbi.replies else []
            if dbi.last_reply:
                dbi.last_reply.created_user
        return db_items

    def topics_info(
        self, 
        db: Session
    ):
        db_items = db.query(models.Topic).all()
        for dbi in db_items:
            dbi.created_user
            dbi.replies
            dbi.last_reply = dbi.replies[-1] if dbi.replies else []
            if dbi.last_reply:
                dbi.last_reply.created_user
        return db_items

    def topic_create(
        self, 
        db: Session, 
        topic: schemas.Topic
    ):
        db_item = models.Topic(**topic.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def topic_update(
        self, 
        db: Session, 
        topic: schemas.Topic
    ):
        db_item = db.query(models.Topic).get(topic.id)
        for key, value in topic:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def topic_delete(
        self, 
        db: Session, 
        topic: schemas.Topic
    ):
        db_item = db.query(models.Topic).get(topic.id)
        db.delete(db_item)
        db.commit()
        return topic

    def reply_get(
        self, 
        db: Session, 
        reply: int
    ):
        return db.query(models.Reply).get(reply)
  
    def reply_create(
        self, 
        db: Session, 
        reply: schemas.Reply
    ):
        db_item = models.Reply(**reply.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def reply_update(
        self, 
        db: Session, 
        reply: schemas.Reply
    ):
        db_item = db.query(models.Reply).get(reply.id)
        for key, value in reply:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def reply_delete(
        self, 
        db: Session, 
        reply: schemas.Reply
    ):
        db_item = db.query(models.Reply).get(reply.id)
        db.delete(db_item)
        db.commit()
        return reply

forum = Forum()
