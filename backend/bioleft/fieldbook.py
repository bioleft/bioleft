import uuid
from typing import List
from fastapi import Depends
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import UUID
import models, schemas
from db.database import SessionLocal, engine, StoreRedis, conn_redis

class Fieldbook():

    def get(self, db, form, project):
        fb = db.query(models.Fieldbook) \
            .filter(models.Fieldbook.project == project, models.Fieldbook.form == form) \
            .first()
        fb.items
        return fb

    async def create_form(self, store, form: schemas.FbForm, values):
        await store.hmset(form.key, values)
    
    async def create_trait(self, store, trait: schemas.Trait):
        await store.hmset(trait.key, trait.dict())

    async def get_fb(self, store, fb: str):
        c = await store.hgetall(fb)
        traits = await store.scan(match='%s:*'%c['key'])

    async def init_fb(self, crop: str):
        store = await StoreRedis(0).conn()
        await store.flushdb()
        crop = schemas.Crop(**{'crop':crop, 'description':'tomate'})
        fbform = schemas.FbForm(**{'form':'evaluacion','obj':'evaluacion 2021'})
        crop.key = 'fb:%s'%crop.crop
        fbform.key = '%s:%s'%(crop.key, fbform.form)
        trait = {
            'key': '%s:sanidad' % fbform.key,
            'title':'sanidad',
            'content':'1=bien 5=mal',
            'type':'numeral',
            'options':'1,2,3,4,5',
            'crop_cycle':'crecimiento',
            'lang':'es'
        }
        trait = schemas.Trait(**trait)
        await store.hmset(crop.key, crop.dict())
        await self.create_form(store, fbform, fbform.dict())
        await self.create_trait(store, trait)
        await self.get_fb(store, fbform.key)

        
fieldbook = Fieldbook()
