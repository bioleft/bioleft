import uuid
from sqlalchemy import func
from typing import List
from fastapi import Depends
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import UUID
import models, schemas

class SeedDB():

    def rel(self, seed):
        seed.owner
        seed.licence
        seed.species
        seed.attributes
        seed.organization
        return seed

class Seed(SeedDB):

    def registration(
        self,
        db: Session,
        user: schemas.User,
        seed: schemas.Seed
    ):
        db_item = models.Seed(**seed.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        db_item.owner
        db_item.licence
        db_item.species
        return db_item

    def add(
        self,
        db: Session,
        user: models.User,
        seed: models.Seed
    ):
        schema_seed = schemas.Seed.from_orm(seed)
        db_item = models.Seed(**schema_seed.dict())
        db_item.owner_id = user.id
        if not db_item.created_from_id:
            db_item.created_from_id = seed.id
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return self.rel(seed)

    def edit(
        self,
        db: Session,
        seed: schemas.Seed
    ):
        db_item = db.query(models.Seed).get(seed.id)
        for key, value in seed:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return self.rel(db_item)

    def remove(
        self,
        db: Session,
        seed: models.Seed
    ):
        db_item = db.query(models.Seed).get(seed.id)
        db.delete(db_item)
        db.commit()
        return db_item

    def filter(
        self,
        db: Session,
        seed: models.Seed
    ):
        db_item = db.query(models.Seed).filter(
            models.Seed.cultivar == seed.cultivar
        ).first()
        return db_item

    def offer(
        self,
        db: Session,
        seed: schemas.Seed,
        transfer: schemas.SeedTransferConfig
    ):
        db_item = models.Seedtransferconfig(**transfer.dict(), seed=seed)
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item

    def update_offer(
        self,
        db: Session,
        seed: schemas.Seed,
        transfer: schemas.SeedTransferConfig
    ):
        db_item = db.query(models.Seedtransferconfig).get(transfer.id)
        for key, value in transfer:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return db_item

    def request(
        self,
        db: Session,
        seed: schemas.Seed,
        request: schemas.SeedTransferRequest
    ):
        db_offer = db.query(models.Seedtransferconfig).filter(
            models.Seedtransferconfig.id == request.offer_id
        ).first()
        db_item = models.Seedtransferrequest(
            user_request_id=request.user_request_id,
            option=db_offer.option,
            seed_quantity=request.seed_quantity,
            seed=seed
        )
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        db_item.seed
        db_item.user_request
        return db_item

    def request_set(
        self,
        db: Session,
        seed: models.Seed,
        request: schemas.SeedTransferRequest
    ):
        db_item = db.query(models.Seedtransferrequest).get(request.id)
        for key, value in request:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        db_item.seed
        db_item.user_request
        return db_item

    def get_seed_requests(
        self,
        db: Session,
        seed: models.Seed
    ):
        db_seed_request = db.query(models.Seedtransferrequest).filter(
            models.Seedtransferrequest.seed == seed
        ).all()
        for request_ in db_seed_request:
            request_.seed
            request_.user_request
        return db_seed_request

    def add_attribute(
        self,
        db: Session,
        seed: models.Seed,
        attr: schemas.Seedattribute
    ):
        db_item = models.Seedattribute(**attr.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item

    def get_attributes(
        self,
        db: Session,
        seed: models.Seed
    ):
        return db.query(models.Seedattribute).filter(
            models.Seedattribute.seed_id==seed.id
        ).all()

    def edit_attribute(
        self,
        db: Session,
        seed: models.Seed,
        attr: schemas.Seedattribute
    ):
        db_item = db.query(models.Seedattribute).get(attr.id)
        for key, value in attr:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return db_item

    def remove_attribute(
        self,
        db: Session,
        seed: models.Seed,
        attr: schemas.Seedattribute
    ):
        db_item = db.query(models.Seedattribute).get(attr.id)
        db.delete(db_item)
        db.commit()
        return db_item

    def set_organization(
        self,
        db: Session,
        seed: models.Seed,
        org: schemas.Organization,
    ):
        seed.organization = org.id
        db.commit()
        db.refresh(seed)
        return seed

    def remove_organization(
        self,
        db: Session,
        seed: models.Seed,
    ):
        seed.organization = None
        db.commit()
        db.refresh(seed)
        return seed

    def get_species(
        self,
        db: Session,
    ):
        return db.query(models.Species).all()

seed = Seed()
