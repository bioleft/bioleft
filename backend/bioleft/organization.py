import uuid
from typing import List
from fastapi import Depends
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import UUID
import models, schemas
from db.database import SessionLocal, engine, StoreRedis, conn_redis
from bioleft import accounts
from bioleft.project import Team

class Organization():

    def create(self,
        db: Session,
        user: models.User,
        org: schemas.Organization
    ):
        org.registered_by_id = user.id
        team = Team.create_team(db=db, user=user)
        org.team_id = team.id
        db_org = models.Organization(**org.dict())
        # ID sequence
        db_org_last = db.query(models.Organization).order_by(models.Organization.id.desc()).first()
        db_org.id = db_org_last.id + 1
        db.add(db_org)
        db.commit()
        db.refresh(db_org)
        return db_org

    def get(self,
        db: Session,
        org: str
    ):
        item = db.query(models.Organization).filter(models.Organization.uuid==org).first()
        item.orgseeds
        item.projects
        for prj in item.projects:
            prj.fieldbook
        item.team
        item.team.users
        for us in item.team.users:
            us.user
        return item

    def get_from_user(self,
        db: Session,
        user: schemas.User
    ):
        items = []
        for ut in user.teams:
            if ut.team.orgs:
                for org in ut.team.orgs:
                    items.append(org)
        return items

    def set_geolocation(
        db: Session,
        org: models.Organization
    ):
        args = dict(reg="ar",loc=str.title(org.geolocation.location),limit=5)
        if 'De' in args['loc'] and args['loc'].index('De') != 0:
            args['loc'] = args['loc'].replace('De', 'de')

        geolocation = service.georef(args)

        if(geolocation):
            db_model_geo = models.Geolocation(
                display_name = geolocation[0]['nombre'],
                location = geolocation[0]['nombre'],
                latitude = str(geolocation[0]['centroide']['lat']),
                longitude = str(geolocation[0]['centroide']['lon']),
            )
            db_item_geo = db.query(models.Geolocation).filter(
                models.Geolocation.location == db_model_geo.location,
                models.Geolocation.display_name == db_model_geo.display_name,
                models.Geolocation.latitude == db_model_geo.latitude,
                models.Geolocation.longitude == db_model_geo.longitude,
            )
            if(db_item_geo.count() == 0):
                db.add(db_model_geo)
                db.commit()
                db.refresh(db_model_geo)
            else:
                db_model_geo = db_item_geo.first()

            org.geolocation = db_model_geo    
            db.commit()
            db.refresh(org)
            org.geolocation
        return org

    def edit(
        self, 
        db: Session, 
        org: schemas.Organization
    ):
        db_item = db.query(models.Organization).get(uuid=org.uuid)
        for key, value in org:
            setattr(db_item, key, value)
        db.commit()
        db.refresh()
        return db_item
    
    def remove(
        self,
        db: Session,
        org: schemas.Organization,
    ):
        db.delete(org)
        db.commit()
        return org

    def user_add(
        self,
        db: Session,
        user: schemas.User,
        org: models.Organization
    ):

        db_item = models.Teamuser(
            user_id=user.id,
            team_id=org.team_id,
            access_level="basic"
        )
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def user_del(
        self,
        db: Session,
        teamuser: schemas.TeamUser,
        org: models.Organization
    ):
        db_item = db.query(models.Teamuser).get(teamuser.id)
        db.delete(db_item)
        db.commit()
        return teamuser

    
organization = Organization()
