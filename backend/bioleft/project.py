import uuid
from typing import List
from fastapi import Depends
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import UUID
import models, schemas
from db.database import SessionLocal, engine, StoreRedis, conn_redis
from bioleft import accounts
from bioleft.fieldbook import fieldbook

class Team():

    def create_team(db, user: schemas.UserInDB):
        team = models.Team(**schemas.Team().dict())
        db.add(team)
        db.commit()
        db.refresh(team)
        team_user = schemas.TeamUser(
            user_id=user.id,
            team_id=team.id,
            access_level="total"
        )
        rel = models.Teamuser(**team_user.dict())
        db.add(rel)
        db.commit()
        db.refresh(rel)
        return team

class Project():

    def create_project(self,
        db: Session,
        user: schemas.UserInDB,
        project: schemas.Project
    ):
        team = Team.create_team(db=db, user=user)
        project.team_id = team.id
        db_project = models.Project(**project.dict())
        db.add(db_project)
        db.commit()
        db.refresh(db_project)
        return db_project

    def edit(
        self,
        db: Session,
        project: schemas.Project
    ):
        db_item = db.query(models.Project).get(project.id)
        for key, value in project:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return db_item

    def remove(
        self,
        db: Session,
        project: models.Project,
    ):
        db.delete(project)
        db.commit()
        return project

    def add_user(
        self,
        db: Session,
        user: schemas.User,
        project: models.Project
    ):
        db_item = models.Teamuser(
            user_id=user.id,
            team_id=project.team_id,
            access_level="basic"
        )
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        db_item.user
        return db_item

    def get_user(
        self,
        db: Session,
        user: schemas.User,
        project: models.Project
    ):
        team_user = db.query(models.Teamuser).filter(
            models.Teamuser.user_id==user.id,
            models.Teamuser.team_id==project.team_id
        )
        return team_user

    def remove_user(
        self,
        db: Session,
        teamuser: models.Teamuser,
        project: models.Project
    ):
        db.delete(teamuser)
        db.commit()
        return teamuser

    def get_project(self, db, uuid):
        db_item = db.query(models.Project).filter(models.Project.uuid == uuid).first()
        db_item.team
        db_item.team.users
        db_item.organization
        db_item.fieldbook
        for users in db_item.team.users:
            users.user
        return db_item

    def get_projects(self, db, user: schemas.UserInDB):
        projects_user = accounts.get_user(db, user.email)
        team_user = db.query(models.Teamuser).filter(models.Teamuser.user==projects_user)
        teams_id = [ team.team_id for team in team_user ]
        db_items = db.query(models.Project).filter(models.Project.team_id.in_(teams_id)).all()
        for item in db_items:
            item.team
            item.team.users
            item.organization
            item.fieldbook
            for users in item.team.users:
                users.user
        return db_items

    def add_fieldbook(self, db: Session, uuid:str, form: schemas.FbForm):
        project = self.get_project(db, uuid)
        form.project_id = project.id
        db_item = models.Fieldbook(**form.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item

    def get_fieldbooks(self, db: Session, uuid):
        project = db.query(models.Project).filter(models.Project.uuid == uuid).first()
        fieldbooks = db.query(models.Fieldbook)\
                        .filter(models.Fieldbook.project == project)\
                        .all()
        for fieldbook in fieldbooks:
            fieldbook.items
        return fieldbooks

    def get_fieldbook(self, db: Session, uuid, form):
        project = db.query(models.Project).filter(models.Project.uuid == uuid).first()
        return fieldbook.get(db, form, project)

    def create_fieldbook_item(
        self,
        db: Session,
        uuid: str,
        form: str,
        item: schemas.FbFormItem
    ):
        project = self.get_project(db, uuid)
        fieldbook_ = db.query(models.Fieldbook).filter(models.Fieldbook.project == project, models.Fieldbook.form == form).first()
        item.fieldbook_id = fieldbook_.id
        db_item = models.Fieldbookitem(**item.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item

    def update_fieldbook_item(
        self,
        db: Session,
        uuid: str,
        form: str,
        item: schemas.FbFormItem
    ):
        project = self.get_project(db, uuid)
        fieldbook_ = db.query(models.Fieldbook).filter(models.Fieldbook.project == project, models.Fieldbook.form == form).first()
        db_item = db.query(models.Fieldbookitem).filter(models.Fieldbookitem.id==item.id).first()
        for key, value in item:
            setattr(db_item, key, value)
        db.commit()
        db.refresh(db_item)
        return item

    def get_fieldbook_item(
        self,
        db: Session,
        uuid: str,
        form: str,
        item: str
    ):
        project = self.get_project(db, uuid)
        fieldbook_ = db.query(models.Fieldbook).filter(models.Fieldbook.form == form, models.Fieldbook.project_id == project.id).first()
        item = db.query(models.Fieldbookitem).filter(models.Fieldbookitem.name == item, models.Fieldbookitem.fieldbook == fieldbook_).first()
        return item

    def delete_fieldbook_item(
        self,
        db: Session,
        uuid: str,
        form: str,
        item: schemas.FbFormItem
    ):
        project = self.get_project(db, uuid)
        fieldbook_ = db.query(models.Fieldbook).filter(models.Fieldbook.project == project, models.Fieldbook.form == form).first()
        db_item = db.query(models.Fieldbookitem).get(item.id)
        db.delete(db_item)
        db.commit()
        return db_item

    def set_organization(
        self,
        db: Session,
        project: models.Project,
        org: schemas.Organization,
    ):
        project.organization = org.id
        db.commit()
        db.refresh(project)
        return project

    def remove_organization(
        self,
        db: Session,
        project: models.Project,
    ):
        project.organization = None
        db.commit()
        db.refresh(project)
        return project

project = Project()
