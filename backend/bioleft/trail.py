from typing import List
import models, schemas

class Trail():

    def get_fieldbook(self, db, user, fieldbook):
        db_fieldbook = db.query(models.Fieldbook).filter(
            models.Fieldbook.uuid==fieldbook
        ).first()
        db_item = db.query(models.Trail).filter(
            models.Trail.fieldbook_id==db_fieldbook.id
        ).all()
        for item in db_item:
            item.creator
            item.trail_seeds
            item.environment
            for ts in item.trail_seeds:
                ts.seed
            item.data
        return db_item

    def get(self, db, user, uuid):
        db_item = db.query(models.Trail).filter(
            models.Trail.uuid==uuid and models.Trail.creator_id==user.id
        ).first()
        db_item.data
        db_item.environment
        db_item.trail_seeds
        db_item.creator
        for ts in db_item.trail_seeds:
            ts.seed
        return db_item

    def init(self, db, user, fieldbook):
        db_trail_last = db.query(models.Trail).order_by(models.Trail.id.desc()).first()
        if db_trail_last == None:
            id = 1
        else:
            id = db_trail_last.id+1
        trail = schemas.Trail(
            id=id,
            creator_id=user.id,
            fieldbook_id=fieldbook
        )
        db_item = models.Trail(**trail.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        db_item.creator
        return db_item

    def environment(self, db, user, trail, env: schemas.TrailEnvironment):
        db_env_last = db.query(models.TrailEnvironment).order_by(
            models.TrailEnvironment.id.desc()
        ).first()
        if db_env_last == None:
            id = 1
        else:
            id = db_env_last.id+1
        env.id = id
        db_item = models.TrailEnvironment(**env.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def add_seed(self, db, user, trail, trailseed: schemas.TrailSeeds):
        db_seed_last = db.query(models.TrailSeeds).order_by(models.TrailSeeds.id.desc()).first()
        if db_seed_last == None:
            id = 1
        else:
            id = db_seed_last.id+1
        trailseed.id = id
        db_item = models.TrailSeeds(**trailseed.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        db_item.seed
        return db_item
    
    def remove_seed(self, db, user, trail, trailseed: schemas.TrailSeeds):
        db_trailseed = db.query(models.TrailSeeds).get(trailseed.id)
        db.delete(db_trailseed)
        db.commit()
        return db_trailseed

    def add_fieldbook_data(self, db, user, trail, data: schemas.TrailFieldbookData):
        db_data_last = db.query(models.TrailFieldbookData).order_by(models.TrailFieldbookData.id.desc()).first()
        if db_data_last == None:
            id = 1
        else:
            id = db_data_last.id+1
        data.id = id
        db_item = models.TrailFieldbookData(**data.dict())
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item
    
    def update_fieldbook_data(self, db, user, trail, data: schemas.TrailFieldbookData):
        db_item = db.query(models.TrailFieldbookData).filter(models.TrailFieldbookData.id==data.id).first()
        for key, value in data:
            setattr(db_item, key, value)
        db.commit()
        return db_item
        
trail = Trail()
