import os

from fastapi import FastAPI
from fastapi import APIRouter, Depends
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
import models, schemas, routers
from bioleft.fieldbook import fieldbook
from db.database import SessionLocal, engine, StoreRedis, conn_redis
from fastapi.middleware.cors import CORSMiddleware

description = """
Plataforma Bioleft
    ## Semillas abiertas
    ## Comunidad
    ## Cuadernos de campo
"""

models.Base.metadata.create_all(bind=engine)

app = FastAPI(
    title="Bioleft",
    description=description,
    version="3.0.0",
    terms_of_service="",
    contact={
        "name": "Bioleft",
        "url": "https://www.bioleft.org",
        "email": "info@bioleft.org",
    },
    license_info={
        "name": "GPLv3",
        "url": "https://www.gnu.org/licenses/gpl-3.0.en.html",
    }
)

origins = [os.getenv('CLIENT_ORIGIN')]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(routers.user.router)
app.include_router(routers.seed.router)
app.include_router(routers.organization.router)
app.include_router(routers.catalog.router)
app.include_router(routers.project.router)
app.include_router(routers.fieldbook.router)
app.include_router(routers.trail.router)
app.include_router(routers.forum.router)
app.include_router(routers.community.router)
app.mount("/static", StaticFiles(directory="static"), name="static")
