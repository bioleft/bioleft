from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)

def user_token():
    response = client.post(
        "/user/token", 
        headers={"content-type": "application/x-www-form-urlencoded"},
        data={
            'username': 'userbiocom',
            'password': 'user1234'
        }
    )
    assert response.status_code == 200
    return response.json()['access_token']

access_token = user_token() 

def test_read_seed():
    response = client.get(
        "/catalog/", 
        headers={"Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200

def test_register():
    response = client.post(
        "/catalog/register", 
        headers={"Authorization": "Bearer "+access_token},
        json={
            'cultivar': 'Testeo',
            'species_id': 1,
        }
    )
    assert response.status_code == 200

def test_offer():
    response = client.post(
        "/catalog/userbiocom/Testeo/offer", 
        headers={"Authorization": "Bearer "+access_token},
        json={
            'option': 'gr',
            'offered': 10,
            'terms': 'Términos'
        }
    )
    assert response.status_code == 200

def test_request():
    response = client.post(
        "/catalog/userbiocom/Testeo/request", 
        headers={"Authorization": "Bearer "+access_token},
        json={
            'offer_id': 1,
            'user_id': 587
        }
    )
    assert response.status_code == 200

def test_send_seed():
    response = client.post(
        "/catalog/send/userbiocom/Testeo", 
        headers={"Authorization": "Bearer "+access_token},
        json={'username': 'adminbioleftorg'}
    )
    assert response.status_code == 200

def test_remove_seed():
    response = client.delete(
        "/catalog/remove/userbiocom/Testeo", 
        headers={"Authorization": "Bearer "+access_token}
    )
    assert response.status_code == 200
