import uvicorn
import os

if __name__ == "__main__":
   os.system('alembic upgrade head')
   uvicorn.run("main:app", host="0.0.0.0", port=8000, log_level="info")
