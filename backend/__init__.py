
# import os
# from flask import Flask
# from flask import request
# from flask import redirect
# from flask import url_for
# from flask import json
# from flask import render_template
# from flask import flash
# import flask_wtf
# from flask_cors import CORS
# from werkzeug.exceptions import HTTPException
# from flask_babelex import Babel
# from flask_mobility import Mobility
# from flask_security import login_required
# from flask_mail import Mail
# from bioleft.db import db_cli, db
# from bioleft.auth import AuthManager
# from bioleft.api.endpoints import api_bp
# from bioleft.views.contexts import *
# from bioleft.views.routes import setup_routes
# from bioleft.models import user
# from flask_jwt_extended import JWTManager
# from flask_smorest import Api

# LANGUAGES = {
#     'en': 'English',
#     'es': 'Español'
# }

# def contexts(app):
#     app.context_processor(decorator_seed_context_options)
#     app.context_processor(decorator_utility)
#     app.context_processor(decorator_cpoption_offered)
#     app.context_processor(decorator_licences)

# def create_app(config=None):
    
#     app = Flask(__name__, instance_relative_config=True)

#     if config is None:
#         from config import Config
#         app.config.from_object(Config)
#     else:
#         app.config.from_mapping(config)

#     jwt = JWTManager(app)
#     CORS(app, origins=['*'])
#     # Images(app)
#     Mobility(app)
#     contexts(app)
#     auth = AuthManager(app)
#     babel = Babel(app)
#     mail = Mail(app)
#     api = Api(app)
#     api.register_blueprint(api_bp)

#     app.mail = mail

#     setup_routes(app)

#     try:
#         os.makedirs(app.instance_path)
#     except OSError:
#         pass

#     @babel.localeselector
#     def get_locale():
#         return request.accept_languages.best_match(LANGUAGES.keys())

#     app.cli.add_command(db_cli)

#     return app
