import os
import json
import shutil
import shutil
from fastapi import APIRouter, Depends, File, UploadFile
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas, models
from bioleft.catalog import catalog
from bioleft.seed import seed
from bioleft import accounts
from fastapi_mail import FastMail, MessageSchema
from utils.mail import EmailConf

router = APIRouter(prefix="/seed")

@router.post("/register")
async def register(
    obj_seed: schemas.Seed,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return seed.registration(db, user=user, seed=obj_seed)

@router.get("/species")
async def get_species(
    db: Session = Depends(get_db),
) -> Any:
    return seed.get_species(db)

@router.get("/crop/{crop}")
async def crop_techsheet(
    crop: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
):
    objs = ""
    filecrop = 'bioleft/crop/%s.json' % (crop)
    if os.path.isfile(filecrop):
        with open(filecrop, 'r') as f:
            data = f.read()
            objs = json.loads(data)
    else:
        with open('bioleft/crop/default.json', 'r') as f:
            data = f.read()
            objs = json.loads(data)
    return objs

@router.post("/send/{account}/{uuid}")
async def send(
    account: str,
    uuid: str,
    user_receive: schemas.User,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    user_receive = accounts.get_user(db, user_receive.username)
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.add(db, user=user_receive, seed=objseed)

@router.delete("/remove/{account}/{uuid}")
async def remove(
    account: str,
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    if (user == objseed.owner):
        return seed.remove(db, seed=objseed)
    else:
        return False

@router.post("/filter_cultivar")
async def seed_edit(
    obj_seed: schemas.Seed,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return seed.filter(db, seed=obj_seed)

@router.put("/{account}/{uuid}")
async def seed_edit(
    account: str,
    uuid: str,
    obj_seed: schemas.Seed,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return seed.edit(db, seed=obj_seed)

@router.post("/{account}/{uuid}/image")
async def upload_image(
    account: str,
    uuid: str,
    file: UploadFile = File(...),
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(accounts.get_current_active_user)
):
    seed_ = catalog.registry(db, account=account, seed=uuid)
    file_location = f"static/content/seed_image_primary/{seed_.uuid}"
    if os.path.isdir(file_location):
        shutil.rmtree(file_location)
    os.makedirs(file_location)
    file_location = file_location + f"/{file.filename}"
    with open(file_location, "wb+") as file_object:
        file_object.write(file.file.read())
    seed_.image_primary = file.filename
    db.commit()
    return seed_

@router.post("/{account}/{uuid}/offer")
async def offer(
    account: str,
    uuid: str,
    transfer: schemas.SeedTransferConfig,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.offer(db, seed=objseed, transfer=transfer)

@router.get("/{account}/{uuid}/offer")
async def get_offer(
    account: str,
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    offered = objseed.offer_config[0] if objseed.offer_config else None
    if offered:
        offered.geolocation
    return offered

@router.put("/{account}/{uuid}/offer")
async def update_offer(
    account: str,
    uuid: str,
    transfer: schemas.SeedTransferConfig,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.update_offer(db, seed=objseed, transfer=transfer)

@router.post("/{account}/{uuid}/request")
async def create_request_seed(
    account: str,
    uuid: str,
    request_seed: schemas.SeedTransferRequest,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    request = seed.request(db, seed=objseed, request=request_seed)
    message = MessageSchema(
        subject="Bioleft: Transacción iniciada",
        recipients=[request.user_request.email],
        template_body=dict(current_user=request.user_request,
                           transfer=request)
    )
    fm = FastMail(EmailConf)
    await fm.send_message(message, template_name="transaction_init.html")
    return request

@router.put("/{account}/{uuid}/request")
async def edit_request(
    account: str,
    uuid: str,
    request_seed: schemas.SeedTransferRequest,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    request = seed.request_set(db, seed=objseed, request=request_seed)
    status = ''
    if request.status == 'success':
        status = 'Aprobada'
    else:
        status = 'Rechazada'
    message = MessageSchema(
        subject="Bioleft: Transacción actualizada",
        recipients=[request.user_request.email],
        template_body=dict(transfer=request, status=status)
    )
    fm = FastMail(EmailConf)
    await fm.send_message(message, template_name="transaction_updated.html")
    return request

@router.get("/{account}/{uuid}/request")
async def get_request_seed(
    account: str,
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    if (user.id == objseed.owner.id or user.id == objseed.creator.id):
        return seed.get_seed_requests(db, seed=objseed)
    else:
        return False

@router.post("/{account}/{uuid}/attribute")
async def add_attribute(
    account: str,
    uuid: str,
    attr: schemas.Seedattribute,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.add_attribute(db, objseed, attr)

@router.get("/{account}/{uuid}/attributes")
async def get_attribute(
    account: str,
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.get_attributes(db, objseed)

@router.put("/{account}/{uuid}/attribute")
async def edit_attribute(
    account: str,
    uuid: str,
    attr: schemas.Seedattribute,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.edit_attribute(db, objseed, attr)

@router.delete("/{account}/{uuid}/attribute")
async def remove_attribute(
    account: str,
    uuid: str,
    attr: schemas.Seedattribute,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.remove_attribute(db, objseed, attr)

@router.post("/{account}/{uuid}/organization")
async def set_organization(
    account: str,
    uuid: str,
    org: schemas.Organization,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.set_organization(db, objseed, org)

@router.delete("/{account}/{uuid}/organization")
async def remove_organization(
    account: str,
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    objseed = catalog.registry(db, account=account, seed=uuid)
    return seed.delete_organization(db, objseed)
