from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas, models
from bioleft import accounts

router = APIRouter(prefix="/community")

class Community():

    def find(
        self,
        db,
        search,
        species,
        activities,
        products_offered
    ):

        community = schemas.Community(
            search=search,
            people=[],
            orgs=[],
            species=species,
            activities=activities,
            products_offered=products_offered
        )

        queries = dict(
            users = db.query(models.User).order_by(
                models.User.name.asc()
            ).filter(
                models.User.name != None
            ),
            orgs = db.query(models.Organization),
            species = db.query(models.Species),
            seeds = db.query(models.Seed)
        )

        self.query(community, queries)
        self.query_user(community, queries)
        self.query_species(community, queries)
        self.query_end(community, queries)

        return {
            'people': community.people,
            'orgs': community.orgs
        }

    def query(self, com, q):
        if(
                len(com.search) == 0 and
                com.species == None
        ):
            return

        check_specie = q['species'].filter(
            models.Species.name.contains(com.search)
        )

        if check_specie.count()>0:
            com.species = com.search
            com.search = None

        if com.species != None:
            q['species'] = check_specie = q['species'].filter(
                models.Species.name.contains(com.species)
            )
            q['seeds'] = q['seeds'].filter(
                models.Seed.species_id.in_([spc.id for spc in q['species']])
            )

    def query_user(self, com, q):
        if com.search:
            q['users'] = q['users'].filter(
                models.User.email.ilike(f'%{com.search}%') |
                models.User.username.ilike(f'%{com.search}%') |
                models.User.name.ilike(f'%{com.search}%')
            )

            q['orgs'] = q['orgs'].filter(
                models.Organization.name.ilike(f'%{com.search}%')
            )

    def query_species(self, com, q):
        if com.species != None:
            usr_seeds = []
            org_seeds = []

            q['species'] = q['species'].filter(models.Species.name.contains(com.species))

            q['seeds'] = q['seeds'].filter(
                models.Seed.species_id.in_([spc.id for spc in q['species']])
            )

            if com.people != []:
                q['seeds'] = q['seeds'].filter(
                    models.Seed.owner_id.in_([usr.id for usr in com.people]),
                    models.Seed.creator_id.in_([usr.id for usr in com.people])
                )

            if com.orgs != []:
                q['seeds'] = q['seeds'].filter(
                    models.Seed.organization_id.in_([org.id for org in com.orgs])
                )

            for seed in q['seeds']:
                if not seed.organization:
                    usr_seeds.append(seed.creator.username)
                else:
                    org_seeds.append(seed.organization.id)

            q['users'] = q['users'].filter(models.User.username.in_(usr_seeds))
            q['orgs'] = q['orgs'].filter(models.Organization.id.in_(org_seeds))

    def query_end(self, com, q):

        if com.activities != None:
            q['users'] = q['users'].filter(
                models.User.interest_activities.ilike(f'%{com.activities}%')
            )
            q['orgs'] = []

        if com.products_offered != None:
            q['users'] = q['users'].filter(models.User.products_offered.ilike(f'%{com.products_offered}%'))
            q['orgs'] = []

        com.people = [ usr for usr in q['users'] ]
        com.orgs = [ org for org in q['orgs'] ]

        for p in com.people:
            p.geolocation
            p.seeds

        for o in com.orgs:
            o.geolocation
            o.orgseeds

@router.get("/data")
async def search(
    search: Optional[str] = None,
    species: Optional[str] = None,
    activities: Optional[str] = None,
    products_offered: Optional[str] = None,
    db: Session = Depends(get_db)
) -> Any:
    instance = Community()

    data = instance.find(
        db,
        search,
        species,
        activities,
        products_offered
    )

    return data
