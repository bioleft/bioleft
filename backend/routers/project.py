from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas, models
from bioleft import fieldbook, accounts
from bioleft.project import project

router = APIRouter(prefix="/project")

@router.get("/")
async def get_projects(
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.get_projects(db, user=user)

@router.post("/create")
async def create(
    project_form: schemas.Project,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.create_project(db, user=user, project=project_form)

@router.get("/{uuid}")
async def get_project(
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.get_project(db=db, uuid=uuid)

@router.put("/{uuid}")
async def edit_project(
    uuid: str,
    prj: schemas.Project,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_project = project.get_project(db=db, uuid=uuid)
    return project.edit(db, prj)

@router.delete("/{uuid}")
async def remove_project(
    uuid: str,
    prj: schemas.Project,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_project = project.get_project(db, uuid=uuid)
    if (project.get_user(db, user, db_project).count()==1):
        return project.remove(db, db_project)
    else:
        return False

@router.post("/{uuid}/team")
async def add_user(
    uuid: str,
    usr: schemas.User,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_project = project.get_project(db=db, uuid=uuid)
    if project.get_user(db, user, db_project).count()==1:
        return project.add_user(db, usr, db_project)
    return False

@router.delete("/{uuid}/team")
async def remove_user(
    uuid: str,
    usr: schemas.User,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_project = project.get_project(db=db, uuid=uuid)
    if project.get_user(db, user, db_project).count()==1:
        prjusr = project.get_user(db, usr, db_project).first()
        return project.remove_user(db, prjusr, db_project)
    return False

@router.post("/{uuid}/fieldbooks")
async def add_fieldbooks(
    uuid: str,
    forms: schemas.FbForm,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.add_fieldbook(db, uuid=uuid, form=forms)

@router.get("/{uuid}/fieldbooks")
async def get_fieldbooks(
    uuid: str,
    db: Session = Depends(get_db),
) -> Any:
    return project.get_fieldbooks(db, uuid)

@router.post("/{uuid}/fieldbook/{fieldbook_name}")
async def create_fieldbook_item(
    uuid: str,
    fieldbook_name: str,
    item: schemas.FbFormItem,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.create_fieldbook_item(db, uuid=uuid, form=fieldbook_name, item=item)

@router.get("/{uuid}/fieldbook/{fieldbook_name}")
async def get_fieldbook(
    uuid: str,
    fieldbook_name: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.get_fieldbook(db, uuid=uuid, form=fieldbook_name)

@router.get("/{uuid}/fieldbook/{fieldbook_name}/{item_name}")
async def get_fieldbook_item(
    uuid: str,
    fieldbook_name: str,
    item_name: str,
    db: Session = Depends(get_db),
) -> Any:
    return project.get_fieldbook_item(db, uuid=uuid, form=fieldbook_name, item=item_name)

@router.put("/{uuid}/fieldbook/{fieldbook_name}/{item_name}")
async def update_fieldbook_item(
    uuid: str,
    fieldbook_name: str,
    item_name: str,
    item: schemas.FbFormItem,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.update_fieldbook_item(db, uuid=uuid, form=fieldbook_name, item=item)

@router.delete("/{uuid}/fieldbook/{fieldbook_name}/{item_name}")
async def delete_fieldbook_item(
    uuid: str,
    fieldbook_name: str,
    item_name: str,
    item: schemas.FbFormItem,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return project.delete_fieldbook_item(db, uuid=uuid, form=fieldbook_name, item=item)

@router.post("/{uuid}/organization")
async def set_organization(
    uuid: str,
    org: schemas.Organization,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_project = project.get_project(db=db, uuid=uuid)
    if project.get_user(db, user, db_project).count()==1:
        return project.set_organization(db, db_project, org)
    return False

@router.delete("/{uuid}/organization")
async def remove_organization(
    account: str,
    seed: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_project = project.get_project(db=db, uuid=uuid)
    if project.get_user(db, user, db_project).count()==1:
        return project.remove_organization(db, db_project)
    return False


@router.get("/user/{usr}")
async def get_projects_user(
    usr: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    user = db.query(models.User).filter(
        models.User.username==usr
    ).first()
    user_schema = schemas.UserInDB(
        username = user.username,
        email = user.email,
        password = user.password,
    )
    return project.get_projects(db, user=user_schema)
