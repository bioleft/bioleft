from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas
from bioleft import accounts
from bioleft.forum import forum, models

router = APIRouter(prefix="/forum")

@router.post("/topic")
async def topic_create(
    topic: schemas.Topic,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    if(user.id != topic.created_user_id):
        return False
    return forum.topic_create(db, topic=topic)

@router.get("/topics/info")
async def topics(
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return forum.topics_info(db)

@router.get("/topic/pinned")
async def topic_pinned(
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    topic_pinned = db.query(models.Topic).order_by(models.Topic.id.asc()).first()
    if topic_pinned:
        topic_pinned.created_user
        topic_pinned.replies
        if topic_pinned.replies:
            topic_pinned.last_reply = topic_pinned.replies[-1]
            if topic_pinned.last_reply:
                topic_pinned.last_reply.created_user
    return topic_pinned

@router.post("/reply")
async def reply_create(
    reply: schemas.Reply,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return forum.reply_create(db, reply=reply)

@router.put("/topic")
async def topic_update(
    topic: schemas.Topic,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    if(user.id != topic.created_user_id):
        return False
    return forum.topic_update(db, topic=topic)

@router.delete("/topic")
async def topic_delete(
    topic: schemas.Topic,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    if(user.id != topic.created_user_id):
        return False
    return forum.topic_delete(db, topic=topic)

@router.put("/reply")
async def reply_update(
    reply: schemas.Reply,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    if(user.id != reply.created_user_id):
        return False
    return forum.reply_update(db, reply=reply)

@router.delete("/reply")
async def reply_delete(
    reply: schemas.Reply,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    if(user.id != reply.created_user_id):
        return False
    return forum.reply_delete(db, reply=reply)

@router.get("/topic/{id}")
async def topic_get(
    id: int,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return forum.topic_get(db, topic=id)

@router.get("/reply/{id}")
async def reply_get(
    id: int,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return forum.reply_get(db, reply=id)

@router.get("/topics/category/{category}")
async def topics_category(
    category: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return forum.topics_category(db, category=category)
