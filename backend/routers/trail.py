from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas
from bioleft import accounts
from bioleft.trail import trail

router = APIRouter(prefix="/trail")

@router.post("/init/{fieldbook}")
async def init(
    fieldbook: int,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.init(db, user=user, fieldbook=fieldbook)

@router.get("/fieldbook/{fieldbook}")
async def get_fieldbook(
    fieldbook: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.get_fieldbook(db, user=user, fieldbook=fieldbook)

@router.get("/{uuid}")
async def get(
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.get(db, user=user, uuid=uuid)

@router.post("/{uuid}/environment")
async def environment(
    uuid: str,
    env: schemas.TrailEnvironment,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.environment(db, user=user, trail=uuid, env=env)

@router.post("/{uuid}/seed")
async def add_seed(
    uuid: str,
    trailseed: schemas.TrailSeeds,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.add_seed(db, user=user, trail=uuid, trailseed=trailseed)

@router.delete("/{uuid}/seed")
async def remove_seed(
    uuid: str,
    trailseed: schemas.TrailSeeds,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.remove_seed(db, user=user, trail=uuid, trailseed=trailseed)

@router.post("/{uuid}/data")
async def add_fieldbook_data(
    uuid: str,
    data: schemas.TrailFieldbookData,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.add_fieldbook_data(db, user=user, trail=uuid, data=data)

@router.put("/{uuid}/data")
async def update_fieldbook_data(
    uuid: str,
    data: schemas.TrailFieldbookData,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return trail.update_fieldbook_data(db, user=user, trail=uuid, data=data)
