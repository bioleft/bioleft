from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas
from bioleft.catalog import catalog
from bioleft.organization import organization
from bioleft import accounts

router = APIRouter(prefix="/org")

@router.post("/create")
async def create(
    org: schemas.Organization,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return organization.create(db, user=user, org=org)

@router.get("/{uuid}")
async def get(
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return organization.get(db, org=uuid)

@router.delete("/{uuid}")
async def remove(
    uuid: str,
    org: schemas.Organization,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    db_org = organization.get(db, org=uuid)
    return organization.remove(db, org=db_org)

@router.post("/{uuid}/useradd")
async def useradd(
    uuid: str,
    usr: schemas.User,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    organization_ = organization.get(db, uuid)
    return organization.user_add(db, user=usr, org=organization_)

@router.delete("/{uuid}/userdel")
async def userdel(
    uuid: str,
    teamuser: schemas.TeamUser,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    organization_ = organization.get(db, uuid)
    return organization.user_del(db, teamuser=teamuser, org=organization_)
