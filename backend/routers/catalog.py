from fastapi import APIRouter, Depends
from pydantic import BaseModel
from typing import Optional, List, Any
from sqlalchemy.orm import Session
from db.database import get_db
import schemas, models
from bioleft.catalog import catalog
from bioleft.seed import seed
from bioleft import accounts

router = APIRouter(prefix="/catalog")

@router.get("/")
async def seeds(
    cultivar: Optional[str] = None,
    species: Optional[str] = None,
    location: Optional[str] = None,
    licence: Optional[int] = None,
    type_: Optional[str] = None,
    order: Optional[str] = None,
    db: Session = Depends(get_db)
) -> Any:
    filters = schemas.Catalog(
        cultivar=cultivar,
        species=species,
        location=location,
        licence=licence,
        type=type_,
        order=order,
    )
    return catalog.seeds(db=db, filters=filters)

@router.get("/uuid/{uuid}")
async def seed_uuid(
    uuid: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return catalog.get_by_uuid(db, uuid)

@router.get("/{account}")
async def by_user(
    account: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    user = db.query(models.User).filter(
        models.User.username==account
    ).first()
    user_schema = schemas.User(id=user.id, username=user.username)
    return catalog.get_from_user(db, user=user_schema)

@router.get("/{account}/{cultivar}")
async def seed(
    account: str,
    cultivar: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
) -> Any:
    return catalog.registry(db, account=account, seed=cultivar)
