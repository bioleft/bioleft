import os.path
import json
from fastapi import APIRouter, Depends
from pydantic import BaseModel
from sqlalchemy.orm import Session
from db.database import get_db
import schemas, models
from bioleft import fieldbook, accounts
from bioleft.project import project

router = APIRouter(prefix="/fieldbook")

@router.get("/{form}/{crop}")
async def form(
    form: str,
    crop: str,
    db: Session = Depends(get_db),
    user: schemas.User = Depends(accounts.get_current_active_user),
):
    objs = ""
    crop = crop.replace('-',' ')
    file = 'bioleft/fieldbook/%s/%s.json' % (form, crop)
    if os.path.exists(file):
        with open(file, 'r') as f:
            data = f.read()
            objs = json.loads(data)
    else:
        with open('bioleft/fieldbook/default/default.json', 'r') as f:
            data = f.read()
            objs = json.loads(data)
    return objs
