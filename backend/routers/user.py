import os
import datetime
from datetime import timedelta
from fastapi import APIRouter, Depends, HTTPException, status, File, UploadFile, Request
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_mail import FastMail, MessageSchema
from db.database import get_db
from sqlalchemy.orm import Session
import schemas, models
from bioleft import accounts
from bioleft.catalog import catalog
from bioleft.seed import seed
from bioleft.organization import organization
from utils.geo import service
from utils.mail import EmailConf

router = APIRouter(prefix="/user")

@router.post("/changepassword")
async def changepassword(
    pw: schemas.UserPassword,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(accounts.get_current_active_user)
):
    if accounts.verify_password(pw.password, current_user.password):
        user_ = accounts.config_password(db, current_user, pw.npassword)
        return {"status": 'ok'}
    return {"status": "error"}

@router.post("/reset")
async def resetpassword(
        request: Request,
        user: schemas.User,
        db: Session = Depends(get_db)
):
    user = db.query(models.User).filter(
        models.User.email==user.email
    ).first()
    npassword = user.username+str(datetime.datetime.now())
    user = accounts.config_password(db, user, npassword)
    message = MessageSchema(
        subject="Recuperación de la cuenta Bioleft",
        recipients=[user.email],
        template_body=dict(
            link = request.headers['origin'] + '/recovery/' + user.username + '-' + user.password.replace('/','_')
        )
    )
    fm = FastMail(EmailConf)
    await fm.send_message(message, template_name="reset_password.html")
    return user

@router.post("/recovery")
async def recovery_password(
        user: schemas.User,
        pw: schemas.UserPassword,
        db: Session = Depends(get_db)
):
    user_ = db.query(models.User).filter(
        models.User.username==user.username
    ).first()
    if user_.password == pw.password.replace('_','/'):
        user = accounts.config_password(db, user_, pw.npassword)
        return user
    return False

@router.post("/token", response_model=schemas.Token)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db)
) -> schemas.Token:
    user = accounts.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Usuario o clave incorrecto",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=accounts.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = accounts.create_access_token(
        data={"sub": user.email}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@router.post("/token_social", response_model=schemas.Token)
async def social_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db)
) -> schemas.Token:
    user = accounts.get_user(db, form_data.username)
    if not user:
        data_user = {
            'username': form_data.username[0: form_data.username.find("@")].replace(".","").replace("-",""),
            'email': form_data.username,
            'password': form_data.password,
        }
        user = schemas.UserInDB(**data_user)
        user = accounts.registry_user(db, user)
    access_token_expires = timedelta(minutes=accounts.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = accounts.create_access_token(
        data={"sub": user.email}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@router.get("/")
async def current_user(
    current_user: schemas.User = Depends(accounts.get_current_active_user)
) -> schemas.User:
    current_user.geolocation
    return current_user

@router.get("/list/{search}")
async def user_list(
    search: str,
    current_user: schemas.User = Depends(accounts.get_current_active_user),
    db: Session = Depends(get_db)
) -> schemas.User:
    return db.query(models.User).filter(
        models.User.username==search
    ).first()

@router.get("/getid/{id}")
async def get_id(
    id: int,
    current_user: schemas.User = Depends(accounts.get_current_active_user),
    db: Session = Depends(get_db)
) -> schemas.User:
    return db.query(models.User).filter(
        models.User.id==id
    ).first()

@router.get("/getusername/{username}")
async def get_username(
    username: str,
    db: Session = Depends(get_db)
) -> schemas.User:
    return db.query(models.User).filter(
        models.User.username==username
    ).first()

@router.post("/config")
async def config_user(
    user: schemas.User,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(accounts.get_current_active_user)
):
    if (user.email == current_user.email):
        return accounts.config_user(db, user)
    return current_user

@router.get("/orgs")
async def orgs_user(
    current_user: schemas.User = Depends(accounts.get_current_active_user),
    db: Session = Depends(get_db),
):
    return organization.get_from_user(db=db, user=current_user)

@router.post("/registry")
async def registry(
    user: schemas.UserInDB,
    db: Session = Depends(get_db),
):
    user_ = db.query(db.query(models.User).filter_by(email=user.email).exists()).scalar()
    if(user_):
        return "El correo ya existe"
    user_ = accounts.registry_user(db=db, user=user)
    message = MessageSchema(
        subject="Bienvenida/o a la comunidad Bioleft",
        recipients=[user_.email],
        template_body=dict(name='')
    )

    fm = FastMail(EmailConf)
    await fm.send_message(message, template_name="welcome.html")
    return user_

@router.get("/requests")
async def requests(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(accounts.get_current_active_user)
):
    return accounts.get_account_requests(db=db, user=current_user)

@router.post("/georef")
async def geo_ref(
    geo: schemas.Geo,
    current_user: schemas.User = Depends(accounts.get_current_active_user),
    db: Session = Depends(get_db),
):
    return service.georef(geo.dict())

@router.post("/image")
async def upload_image(
    file: UploadFile = File(...),
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(accounts.get_current_active_user)
):

    file_location = accounts.folder_static_avatar + file.filename
    with open(file_location, "wb+") as file_object:
        file_object.write(file.file.read())
    current_user.avatar = '/' + file.filename
    db.commit()
    return current_user.avatar
