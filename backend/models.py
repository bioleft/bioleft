import datetime
import uuid
from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, Integer, String, Text, text, Index, BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
from db.database import Base
from sqlalchemy_serializer import SerializerMixin

metadata = Base.metadata

class Geolocation(Base):
    __tablename__ = 'geolocations'

    id = Column(Integer, primary_key=True)
    city = Column(String(255))
    country = Column(String(255))
    location = Column(String(255), nullable=False)
    latitude = Column(String(255))
    longitude = Column(String(255))
    region = Column(String(255))
    display_name = Column(String(255), nullable=False)
    state_district = Column(String(255))
    state = Column(String(255))


class Licence(Base):
    __tablename__ = 'licence'

    id = Column(Integer, primary_key=True)
    name = Column(String(256), nullable=False)
    detail = Column(Text, nullable=False)
    image = Column(String(255), nullable=False)


class Migratehistory(Base):
    __tablename__ = 'migratehistory'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    migrated = Column(DateTime, nullable=False)


class Role(Base):
    __tablename__ = 'role'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)


class Seedattributes(Base):
    __tablename__ = 'seedattributes'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    relevant = Column(Boolean, nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    group = Column(String(250))
    options = Column(String(250))
    description = Column(String(250))


class Seedcategory(Base):
    __tablename__ = 'seedcategory'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    subcategory_id = Column(ForeignKey('seedcategory.id'), index=True)

    subcategory = relationship('Seedcategory', remote_side=[id])


class Seedtransfersetting(Base):
    __tablename__ = 'seedtransfersettings'

    id = Column(Integer, primary_key=True)
    terms = Column(Text, nullable=False)

class Species(Base):
    __tablename__ = 'species'

    id = Column(Integer, primary_key=True)
    name = Column(String(256), nullable=False)
    image = Column(String(256))

class Team(Base):
    __tablename__ = 'team'

    id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)

class Seedattributespecies(Base):
    __tablename__ = 'seedattributespecies'

    id = Column(Integer, primary_key=True)
    species_id = Column(ForeignKey('species.id', ondelete="CASCADE"), index=True)
    attribute_id = Column(ForeignKey('seedattributes.id', ondelete='CASCADE'), index=True)

    attribute = relationship('Seedattributes')
    species = relationship('Species')

class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(255), unique=True)
    name = Column(String(255))
    email = Column(String(255), nullable=False, unique=True)
    password = Column(Text, nullable=False)
    active = Column(Boolean, default=True, nullable=False)
    confirmed_at = Column(DateTime)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    avatar = Column(String(255))
    type_ = Column(String(255))
    contact = Column(String(30))
    bio = Column(Text)
    admin = Column(Boolean)
    geolocation_id = Column(ForeignKey('geolocations.id', ondelete="CASCADE"), index=True)
    interest_activities = Column(String(255))
    interest_crop = Column(String(255))
    products_offered = Column(String(255))
    geolocation = relationship('Geolocation')

class Organization(Base):
    __tablename__ = 'organization'

    id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)
    name = Column(String(255), nullable=False)
    registered_by_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    avatar = Column(String(255))
    team_id = Column(ForeignKey('team.id'), index=True)
    bio = Column(String(250))
    site_url = Column(String(255))
    type = Column(String(255))
    geolocation_id = Column(ForeignKey('geolocations.id', ondelete="CASCADE"), index=True)

    geolocation = relationship('Geolocation')
    registered_by = relationship('User')
    team = relationship('Team', backref="orgs")

class Teamuser(Base):
    __tablename__ = 'teamuser'

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    team_id = Column(ForeignKey('team.id', ondelete="CASCADE"), index=True)
    access_level = Column(String(255))

    team = relationship('Team', backref="users")
    user = relationship('User', backref="teams")

class Topic(Base):
    __tablename__ = 'topic'
    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    created_user_id = Column(ForeignKey('user.id', ondelete='CASCADE'), index=True)
    visits = Column(Integer, nullable=False)
    content = Column(Text, nullable=False)
    category = Column(String(255), nullable=False)
    created_user = relationship('User')

class Userrole(Base):
    __tablename__ = 'userroles'
    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    role_id = Column(ForeignKey('role.id', ondelete="CASCADE"), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    role = relationship('Role')
    user = relationship('User')

class Project(Base):
    __tablename__ = 'project'
    id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)
    name = Column(String(255), nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    team_id = Column(ForeignKey('team.id', ondelete="CASCADE"), index=True)
    organization_id = Column(ForeignKey('organization.id', ondelete='CASCADE'), index=True)
    managment = Column(String(255), nullable=True)
    description = Column(Text, nullable=True)
    traits_advances = Column(Boolean, default=False)
    traits_basics = Column(Boolean, default=False)

    organization = relationship('Organization', backref="projects")
    team = relationship('Team' , backref="team")


class Reply(Base):
    __tablename__ = 'reply'

    id = Column(Integer, primary_key=True)
    topic_id = Column(ForeignKey('topic.id', ondelete='CASCADE'), index=True)
    content = Column(Text, nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    created_user_id = Column(ForeignKey('user.id', ondelete='CASCADE'), index=True)

    created_user = relationship('User')
    topic = relationship('Topic', backref='replies')


class Seed(Base):
    __tablename__ = 'seed'

    id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)
    cultivar = Column(String(250), nullable=False)
    breeder = Column(String(56))
    image_primary = Column(String(255))
    category_id = Column(ForeignKey('seedcategory.id', ondelete="CASCADE"), index=True)
    species_id = Column(ForeignKey('species.id', ondelete="CASCADE"), index=True)
    creator_id = Column(ForeignKey('user.id', ondelete='CASCADE'), index=True)
    owner_id = Column(ForeignKey('user.id', ondelete='CASCADE'), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    transfer_settings_id = Column(ForeignKey('seedtransfersettings.id', ondelete="CASCADE"), index=True)
    licence_id = Column(ForeignKey('licence.id', ondelete="CASCADE"), index=True)
    type_ = Column(String(255))
    created_from_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    is_transferred = Column(Boolean, default=False, nullable=False)
    is_availables = Column(Boolean, default=False, nullable=False)
    organization_id = Column(ForeignKey('organization.id', ondelete='CASCADE'), index=True)
    detail = Column(Text)
    experimental = Column(Boolean, default=False, nullable=False)
    visible_search = Column(Boolean, default=True, nullable=False)

    category = relationship('Seedcategory')
    created_from = relationship('Seed', remote_side=[id])
    creator = relationship('User', backref="cseeds", primaryjoin='Seed.creator_id == User.id')
    licence = relationship('Licence', lazy="select")
    organization = relationship('Organization', backref="orgseeds", lazy="select")
    owner = relationship('User', backref="seeds", primaryjoin='Seed.owner_id == User.id', lazy="select")
    species = relationship('Species', backref="seeds", lazy="select")
    transfer_settings = relationship('Seedtransfersetting', lazy="select")


class Fieldbook(Base):
    __tablename__ = 'fieldbook'

    id = Column(Integer, primary_key=True)
    uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    project_id = Column(ForeignKey('project.id', ondelete='CASCADE'), index=True, nullable=True)
    managment = Column(String(255), nullable=True)
    form = Column(String(255))
    obj = Column(String(255), nullable=True)
    title = Column(String(255), nullable=True)
    crop = Column(String(255))
    project = relationship('Project', backref="fieldbook")


class Projectseed(Base):
    __tablename__ = 'projectseeds'

    id = Column(Integer, primary_key=True)
    project_id = Column(ForeignKey('project.id', ondelete='CASCADE'), index=True)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)

    project = relationship('Project', lazy="select")
    seed = relationship('Seed', lazy="select")


class Seedattribute(Base):
    __tablename__ = 'seedattribute'

    id = Column(Integer, primary_key=True)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    attribute_id = Column(ForeignKey('seedattributes.id', ondelete='CASCADE'), index=True)
    attribute_by_user = Column(String(255), nullable=True)
    name = Column(String(255), nullable=True)
    value = Column(String(250), nullable=False)
    unit = Column(String(255), nullable=True)

    attribute = relationship('Seedattributes', backref="dbattr", lazy="select")
    seed = relationship('Seed', backref="attributes", lazy="select")

class Seedgeneration(Base):
    __tablename__ = 'seedgeneration'

    id = Column(Integer, primary_key=True)
    generation = Column(Integer, nullable=False)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    creator_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)

    creator = relationship('User')
    seed = relationship('Seed')


class Seedtransferconfig(Base):
    __tablename__ = 'seedtransferconfig'

    id = Column(Integer, primary_key=True)
    option = Column(String(255))
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    offered = Column(Integer)
    offered_per_person = Column(Integer)
    terms = Column(Text)
    location = Column(String(255), nullable=True)
    geolocation_id = Column(ForeignKey('geolocations.id', ondelete="CASCADE"), index=True)

    geolocation = relationship('Geolocation', lazy="select")
    seed = relationship('Seed', backref='offer_config', lazy="select")

class Seedtransferpoint(Base):
    __tablename__ = 'seedtransferpoint'

    id = Column(Integer, primary_key=True)
    geolocation_id = Column(ForeignKey('geolocations.id', ondelete="CASCADE"), index=True)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    offered = Column(Integer, nullable=False)
    delivered = Column(Integer)
    option = Column(String(255), nullable=False)
    creator_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)

    creator = relationship('User')
    geolocation = relationship('Geolocation')
    seed = relationship('Seed')

class Seedtransferrequest(Base, SerializerMixin):
    __tablename__ = 'seedtransferrequest'

    id = Column(Integer, primary_key=True)
    status = Column(String(255), default="standby", nullable=False)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    seed_quantity = Column(Integer)
    user_request_id = Column(ForeignKey('user.id', ondelete='CASCADE'), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    option = Column(String(255))
    comment = Column(String(255))

    seed = relationship('Seed', lazy="select")
    user_request = relationship('User', backref="requests", lazy="select")


class Fieldbookitem(Base):
    __tablename__ = 'fieldbookitem'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    alias = Column(String(255))
    description = Column(Text)
    title = Column(String(255))
    text_label = Column(String(255))
    type_format = Column(String(255), nullable=False)
    fieldbook_id = Column(ForeignKey('fieldbook.id', ondelete='CASCADE'), index=True)
    category_format = Column(String(255))
    optional = Column(Boolean, default=False)
    enable = Column(Boolean, default=True)
    options = Column(String(600))
    crop_cycle = Column(String(600))
    form_order = Column(Integer)

    fieldbook = relationship('Fieldbook', backref='items', lazy='select')


class Trait(Base):
    __tablename__ = 'trait'

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    creator_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    fieldbook_id = Column(ForeignKey('fieldbook.id', ondelete='CASCADE'), index=True)
    env_outdoor_container = Column(Boolean, nullable=False)
    plots = Column(Integer, nullable=False)
    env_to_field = Column(Boolean, nullable=False)
    env_greenhouse_soil = Column(Boolean, nullable=False)
    env_greenhouse_container = Column(Boolean, nullable=False)
    title = Column(String(255))

    creator = relationship('User', lazy="select")
    fieldbook = relationship('Fieldbook', lazy="select")

class Trail(Base):
    __tablename__ = 'trail'

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    creator_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    fieldbook_id = Column(ForeignKey('fieldbook.id', ondelete='CASCADE'), index=True)
    name = Column(String(255))
    uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, nullable=False)

    creator = relationship('User', lazy="select")
    fieldbook = relationship('Fieldbook', backref="trails", lazy="select")

class TrailEnvironment(Base):
    __tablename__ = 'environmenttrail'

    id = Column(Integer, primary_key=True)
    trail_id = Column(ForeignKey('trail.id', ondelete='CASCADE'), index=True)
    value = Column(String(255))

    trail = relationship('Trail', backref="environment", lazy="select")

class TrailSeeds(Base):
    __tablename__ = 'trailseeds'

    id = Column(Integer, primary_key=True)
    trail_id = Column(ForeignKey('trail.id', ondelete='CASCADE'), index=True)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)

    seed = relationship('Seed', backref="seeds")
    trail = relationship('Trail', backref="trail_seeds", lazy="select")

class TrailFieldbookData(Base):
    __tablename__ = 'trailfieldbookdata'

    id = Column(Integer, primary_key=True)
    item_id = Column(ForeignKey('fieldbookitem.id', ondelete='CASCADE'), index=True)
    trail_id = Column(ForeignKey('trail.id', ondelete='CASCADE'), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    date_observation = Column(DateTime)
    creator_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    trailseeds_id = Column(ForeignKey('trailseeds.id', ondelete='CASCADE'), index=True)
    value = Column(Text, nullable=False)
    creator = relationship('User')
    item = relationship('Fieldbookitem')
    trailseeds = relationship('TrailSeeds')
    trail = relationship('Trail', backref="data", lazy="select")

class Environmenttrait(Base):
    __tablename__ = 'environmenttrait'

    id = Column(Integer, primary_key=True)
    soil_type = Column(String(255))
    geolocation_id = Column(ForeignKey('geolocations.id', ondelete="CASCADE"), index=True)
    trait_id = Column(ForeignKey('trait.id', ondelete='CASCADE'), index=True)
    plants_per_row = Column(Integer)
    density = Column(Integer)
    surface = Column(String(255))
    total_plants = Column(Integer)
    substrate = Column(String(250))
    rows = Column(Integer)
    plot_id = Column(Integer)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    planting_date = Column(Date)
    transplant_date = Column(Date)
    crop_initial_info = Column(String(255))

    geolocation = relationship('Geolocation', lazy="select")
    seed = relationship('Seed', lazy="select")
    trait = relationship('Trait', lazy="select")


class Fieldbookitemdisabled(Base):
    __tablename__ = 'fieldbookitemdisabled'

    id = Column(Integer, primary_key=True)
    item_id = Column(ForeignKey('fieldbookitem.id', ondelete="CASCADE"), index=True)
    fieldbook_id = Column(ForeignKey('fieldbook.id', ondelete="CASCADE"), index=True)

    fieldbook = relationship('Fieldbook')
    item = relationship('Fieldbookitem')


class Traitseed(Base):
    __tablename__ = 'traitseeds'

    id = Column(Integer, primary_key=True)
    trait_id = Column(ForeignKey('trait.id', ondelete='CASCADE'), index=True)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)

    seed = relationship('Seed')
    trait = relationship('Trait')


class Traitdata(Base):
    __tablename__ = 'traitdata'

    id = Column(Integer, primary_key=True)
    item_id = Column(ForeignKey('fieldbookitem.id', ondelete='CASCADE'), index=True)
    trait_id = Column(ForeignKey('trait.id', ondelete='CASCADE'), index=True)
    created_date = Column(DateTime, default=datetime.datetime.now, nullable=False)
    updated_date = Column(DateTime)
    creator_id = Column(ForeignKey('user.id', ondelete="CASCADE"), index=True)
    seed_id = Column(ForeignKey('seed.id', ondelete='CASCADE'), index=True)
    environment_plant = Column(Integer)
    environment_row = Column(Integer)
    environment_id = Column(ForeignKey('environmenttrait.id', ondelete="CASCADE"), index=True)
    value = Column(Text, nullable=False)

    creator = relationship('User')
    environment = relationship('Environmenttrait')
    item = relationship('Fieldbookitem')
    seed = relationship('Seed')
    trait = relationship('Trait')


class Fieldbookitemhistory(Base):
    __tablename__ = 'fieldbookitemhistory'

    id = Column(Integer, primary_key=True)
    item_id = Column(ForeignKey('fieldbookitem.id', ondelete="CASCADE"), index=True)
    trait_data_id = Column(ForeignKey('traitdata.id', ondelete="CASCADE"), index=True)

    item = relationship('Fieldbookitem')
    trait_data = relationship('Traitdata')

Index('environmenttrait_geolocation_id', Environmenttrait.geolocation_id)
Index('environmenttrait_seed_id', Environmenttrait.seed_id)
Index('environmenttrait_trait_id', Environmenttrait.trait_id)
Index('fieldbook_project_id', Fieldbook.project_id)
Index('fieldbookitem_fieldbook_id', Fieldbookitem.fieldbook_id)
Index('fieldbookitemdisabled_fieldbook_id', Fieldbookitemdisabled.fieldbook_id)
Index('fieldbookitemdisabled_item_id', Fieldbookitemdisabled.item_id)
Index('fieldbookitemhistory_item_id', Fieldbookitemhistory.item_id)
Index('fieldbookitemhistory_trait_data_id', Fieldbookitemhistory.trait_data_id)
Index('organization_geolocation_id', Organization.geolocation_id)
Index('organization_registered_by_id', Organization.registered_by_id)
Index('organization_team_id', Organization.team_id)
Index('project_organization_id', Project.organization_id)
Index('project_team_id', Project.team_id)
Index('projectseeds_project_id', Projectseed.project_id)
Index('projectseeds_seed_id', Projectseed.seed_id)
Index('reply_created_user_id', Reply.created_user_id)
Index('reply_topic_id', Reply.topic_id)
Index('role_name', Role.name)
Index('seed_category_id', Seed.category_id)
Index('seed_created_from_id', Seed.created_from_id)
Index('seed_creator_id', Seed.creator_id)
Index('seed_licence_id', Seed.licence_id)
Index('seed_organization_id', Seed.organization_id)
Index('seed_owner_id', Seed.owner_id)
Index('seed_species_id', Seed.species_id)
Index('seed_transfer_settings_id', Seed.transfer_settings_id)
Index('seedattribute_attribute_id', Seedattribute.attribute_id)
Index('seedattribute_seed_id', Seedattribute.seed_id)
Index('seedattributespecies_attribute_id', Seedattributespecies.attribute_id)
Index('seedattributespecies_species_id', Seedattributespecies.species_id)
Index('seedcategory_subcategory_id', Seedcategory.subcategory_id)
Index('seedgeneration_creator_id', Seedgeneration.creator_id)
Index('seedgeneration_seed_id', Seedgeneration.seed_id)
Index('seedtransferconfig_geolocation_id', Seedtransferconfig.geolocation_id)
Index('seedtransferconfig_seed_id', Seedtransferconfig.seed_id)
Index('seedtransferpoint_creator_id', Seedtransferpoint.creator_id)
Index('seedtransferpoint_geolocation_id', Seedtransferpoint.geolocation_id)
Index('seedtransferpoint_seed_id', Seedtransferpoint.seed_id)
Index('seedtransferrequest_seed_id', Seedtransferrequest.seed_id)
Index('seedtransferrequest_user_request_id', Seedtransferrequest.user_request_id)
Index('teamuser_team_id', Teamuser.team_id)
Index('teamuser_user_id', Teamuser.user_id)
Index('topic_created_user_id', Topic.created_user_id)
Index('trait_creator_id', Trait.creator_id)
Index('trait_fieldbook_id', Trait.fieldbook_id)
Index('trail_creator_id', Trail.creator_id)
Index('trail_fieldbook_id', Trail.fieldbook_id)
Index('trailenvironment_trail_id', TrailEnvironment.trail_id)
Index('traitdata_creator_id', Traitdata.creator_id)
Index('traitdata_environment_id', Traitdata.environment_id)
Index('traitdata_item_id', Traitdata.item_id)
Index('traitdata_seed_id', Traitdata.seed_id)
Index('traitdata_trait_id', Traitdata.trait_id)
Index('traitseeds_seed_id', Traitseed.seed_id)
Index('traitseeds_trait_id', Traitseed.trait_id)
Index('trailseeds_seed_id', TrailSeeds.seed_id)
Index('trailseeds_trail_id', TrailSeeds.trail_id)
Index('trailfieldbookdata_item_id', TrailFieldbookData.item_id)
Index('trailfieldbookdata_trailseeds_id', TrailFieldbookData.trailseeds_id)
Index('trailfieldbookdata_trail_id', TrailFieldbookData.trail_id)
Index('user_email', User.email)
Index('user_geolocation_id', User.geolocation_id)
Index('user_username', User.username)
Index('userroles_role_id', Userrole.role_id)
Index('userroles_user_id', Userrole.user_id)
