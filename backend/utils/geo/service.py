import json

def georef(args):
    locs = []
    with open('utils/geo/ref/%s/loc.json' % args['reg'], 'r') as f:
        data = f.read()
        objs = json.loads(data)

    if 'loc' not in args:
        locs = objs['localidades-censales']
    else:
        if args['reg'] == "ar":
            locs = [ loc for loc in objs['localidades-censales'] if loc['nombre'].lower().find(args['loc'].lower()) != -1 ]
    if args['limit'] > 0:
        locs = locs[:args['limit']]
    return locs
