from pathlib import Path
import os
from fastapi_mail import ConnectionConfig
from dotenv import load_dotenv

load_dotenv()

EmailConf = ConnectionConfig(
    MAIL_USERNAME = os.getenv('MAIL_USERNAME'),
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD'),
    MAIL_FROM = os.getenv('MAIL_USERNAME'),
    MAIL_PORT = os.getenv('MAIL_PORT'),
    MAIL_SERVER = os.getenv('MAIL_SERVER'),
    MAIL_TLS = os.getenv('MAIL_TLS'),
    MAIL_SSL = os.getenv('MAIL_SSL'),
    USE_CREDENTIALS = True,
    VALIDATE_CERTS = False,
    TEMPLATE_FOLDER = Path(__file__).parent / 'templates'
)
