## Bioleft

Esta es la plataforma de [Bioleft.org](https://bioleft.org) para semillas de código abierto, la comunidad de Bioleft y el mejoramiento participativo.

Podés ver los [tutoriales de usuario](https://bioleft.org/es/plataforma) que son guías de uso de la plataforma.

Para instalarlo y desarrollar podés ir a [los documentos de desarrollador](https://gitlab.com/bioleft/bioleft/-/wikis/Desarrollo).

Para contribuir con la plataforma podés leer [como contribuir](https://bioleft.org/es/plataforma).
